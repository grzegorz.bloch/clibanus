#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <stdarg.h>
#include <cmocka.h>


void testInitArray(void **state);
void testReleaseArray(void **state);
void testMakeArray(void **state);
void testMakeSizedArray(void **state);
void testCopyArray(void **state);
void testCopyReleasedArray(void **state);
void testAddToArray(void **state);
void testAddByPtrToArray(void **state);
void testReduceArray(void **state);

void testInitQueue(void **state);
void testReleaseQueue(void **state);
void testMakeQueue(void **state);
void testMakeSizedQueue(void **state);
void testCopyQueue(void **state);
void testCopyReleasedQueue(void **state);
void testAddToQueue(void **state);
void testAddByPtrToQueue(void **state);
void testRemoveFromQueue(void **state);
void testExtendStraightQueue(void **state);
void testExtendRoundedQueue(void **state);
void testContractStraightQueue(void **state);
void testContractRoundedQueue(void **state);
void testGetFromQueue(void **state);
void testGetPtrFromQueue(void **state);

void testFindMapCapacityIndex(void **state);
void testInitMap(void **state);
void testReleaseMap(void **state);
void testMakeMap(void **state);
void testMakeSizedMap(void **state);
void testCopyMap(void **state);
void testAddToMapExtends(void **state);
void testRemoveFromMapReduces(void **state);
void testAddToMapCollision(void **state);
void testRemoveFromMapCluster(void **state);
void testGetFromMap(void **state);
void testKeyByPtr(void **state);
void testMapIterator(void **state);
void testRemoveMapItemAndReduceMap(void **state);

void testInitDset(void **state);
void testReleaseDset(void **state);
void testMakeDset(void **state);
void testMakeSizedDset(void **state);
void testCopyDset(void **state);
void testAddToDset(void **state);
void testGetFromDset(void **state);
void testUnionDset(void **state);

void testInitChannel(void **state);
void testReleaseChannel(void **state);
void testMakeChannel(void **state);
void testAddToChannel(void **state);
void testAddByPtrToChannel(void **state);
void testGetFromChannel(void **state);


int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(testInitArray),
        cmocka_unit_test(testReleaseArray),
        cmocka_unit_test(testMakeArray),
        cmocka_unit_test(testMakeSizedArray),
        cmocka_unit_test(testCopyArray),
        cmocka_unit_test(testCopyReleasedArray),
        cmocka_unit_test(testAddToArray),
        cmocka_unit_test(testAddByPtrToArray),
        cmocka_unit_test(testReduceArray),

        cmocka_unit_test(testInitQueue),
        cmocka_unit_test(testReleaseQueue),
        cmocka_unit_test(testMakeQueue),
        cmocka_unit_test(testMakeSizedQueue),
        cmocka_unit_test(testCopyQueue),
        cmocka_unit_test(testCopyReleasedQueue),
        cmocka_unit_test(testAddToQueue),
        cmocka_unit_test(testAddByPtrToQueue),
        cmocka_unit_test(testRemoveFromQueue),
        cmocka_unit_test(testExtendStraightQueue),
        cmocka_unit_test(testExtendRoundedQueue),
        cmocka_unit_test(testContractStraightQueue),
        cmocka_unit_test(testContractRoundedQueue),
        cmocka_unit_test(testGetFromQueue),
        cmocka_unit_test(testGetPtrFromQueue),

        cmocka_unit_test(testFindMapCapacityIndex),
        cmocka_unit_test(testInitMap),
        cmocka_unit_test(testReleaseMap),
        cmocka_unit_test(testMakeMap),
        cmocka_unit_test(testMakeSizedMap),
        cmocka_unit_test(testCopyMap),
        cmocka_unit_test(testAddToMapExtends),
        cmocka_unit_test(testRemoveFromMapReduces),
        cmocka_unit_test(testAddToMapCollision),
        cmocka_unit_test(testRemoveFromMapCluster),
        cmocka_unit_test(testGetFromMap),
        cmocka_unit_test(testKeyByPtr),
        cmocka_unit_test(testMapIterator),
        cmocka_unit_test(testRemoveMapItemAndReduceMap),

        cmocka_unit_test(testInitDset),
        cmocka_unit_test(testReleaseDset),
        cmocka_unit_test(testMakeDset),
        cmocka_unit_test(testMakeSizedDset),
        cmocka_unit_test(testCopyDset),
        cmocka_unit_test(testAddToDset),
        cmocka_unit_test(testGetFromDset),
        cmocka_unit_test(testUnionDset),

        cmocka_unit_test(testInitChannel),
        cmocka_unit_test(testReleaseChannel),
        cmocka_unit_test(testMakeChannel),
        cmocka_unit_test(testAddToChannel),
        cmocka_unit_test(testAddByPtrToChannel),
        cmocka_unit_test(testGetFromChannel)
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}

