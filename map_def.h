#ifndef MAP_DEFINE_H
#define MAP_DEFINE_H

/**
 * All common Map definitions
 */


#include "token.h"


#ifndef MAP
#define MAP MAKE_SEPARATED_TOKEN_3(Map, MAP_KEY, MAP_VALUE)
#endif /* MAP */


#ifndef MAP_ITEM
#define MAP_ITEM MAKE_SEPARATED_TOKEN_3(MapItem, MAP_KEY, MAP_VALUE)
#endif /* MAP_ITEM */


#ifndef MAP_ITERATOR
#define MAP_ITERATOR MAKE_TOKEN(Iterator, MAP)
#endif /* MAP_ITERATOR */


/* Key passed by value only. */
#define SMALL_KEY_HASH(key, capacity) (((size_t) (key)) % (capacity))

#define BIG_KEY_HASH MAKE_TOKEN(bigKeyHash, MAP)

#ifdef EXTERNAL_KEY_HASH
#define KEY_HASH EXTERNAL_KEY_HASH
#else /* EXTERNAL_KEY_HASH */
#define KEY_HASH SMALL_KEY_HASH
#endif /* EXTERNAL_KEY_HASH */


/* Key passed by value only. */
#define SMALL_KEYS_EQUAL(keyl, keyr) ((keyl) == (keyr))


#ifdef KEY_BY_PTR
#define BIG_KEYS_EQUAL(keyl, keyr) (memcmp((keyl), (keyr), sizeof(MAP_KEY)) == 0)
#else
#define BIG_KEYS_EQUAL(keyl, keyr) (memcmp(&(keyl), &(keyr), sizeof(MAP_KEY)) == 0)
#endif

#ifdef EXTERNAL_KEYS_EQUAL
#define KEYS_EQUAL EXTERNAL_KEYS_EQUAL
#else /* EXTERNAL_KEYS_EQUAL */
#define KEYS_EQUAL SMALL_KEYS_EQUAL
#endif /* EXTERNAL_KEYS_EQUAL */


#define INIT MAKE_TOKEN(init, MAP)

#define RELEASE MAKE_TOKEN(release, MAP)

#define MAKE MAKE_TOKEN(make, MAP)

#define MAKE_SIZED MAKE_TOKEN(makeSized, MAP)

#define COPY MAKE_TOKEN(copy, MAP)

#define ADD MAKE_TOKEN(addTo, MAP)

#define GET_ITEM MAKE_TOKEN(getItemFrom, MAP)

#define GET_VALUE_PTR MAKE_TOKEN(getValuePtrFrom, MAP)

#define GET_VALUE MAKE_TOKEN(getValueFrom, MAP)

#define REMOVE MAKE_TOKEN(removeFrom, MAP)

#define CLEAR MAKE_TOKEN(clear, MAP)

#define REDUCE MAKE_TOKEN(reduce, MAP)

#define GET_ITERATOR MAKE_TOKEN(getIterator, MAP)

#define NEXT_ITEM MAKE_TOKEN(next, MAP_ITEM)

#define REMOVE_ITEM MAKE_TOKEN(remove, MAP_ITEM)


#endif /* MAP_DEFINE_H */
