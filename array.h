/**
 * Generically typed, continuous, dynamic array.
 */

#include "array_def.h"

#include <stddef.h>


struct ARRAY {
    ARRAY_ITEM *items;
    size_t size;
    size_t capacity;
};
typedef struct ARRAY ARRAY;


int INIT_ARRAY (
    ARRAY *a,
    size_t size,
    size_t capacity
);

void RELEASE_ARRAY(ARRAY *a);

ARRAY MAKE_ARRAY(void);

ARRAY MAKE_SIZED_ARRAY(size_t size);

ARRAY COPY_ARRAY(ARRAY *a);

ARRAY_ITEM *ADD_TO_ARRAY(
    ARRAY *a,
    ARRAY_ITEM item
);

ARRAY_ITEM *ADD_BY_PTR_TO_ARRAY(
    ARRAY *a,
    ARRAY_ITEM *item
);

ARRAY_ITEM *ADD_EMPTY_TO_ARRAY(ARRAY *a);


int REDUCE_ARRAY(ARRAY *a);


int RESIZE_ARRAY(
    ARRAY *a,
    size_t size
);


#include "array_undef.h"
