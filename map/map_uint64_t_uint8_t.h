#ifndef MAP_UINT64_T_UINT8_T
#define MAP_UINT64_T_UINT8_T


#define MAP_KEY uint64_t
#define MAP_VALUE uint8_t

#include "../map.h"


#endif /* MAP_UINT64_T_UINT8_T */
