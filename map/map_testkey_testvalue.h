#ifndef MAP_TEST_KEY_TEST_VALUE
#define MAP_TEST_KEY_TEST_VALUE


struct TestKey {
    long l;
    float f;
};
typedef struct TestKey TestKey;

struct TestValue{
    char c;
    int i;
};
typedef struct TestValue TestValue;


#define MAP_KEY TestKey
#define MAP_VALUE TestValue

#define KEY_BY_PTR
#define VALUE_BY_PTR

#define EXTERNAL_KEY_HASH BIG_KEY_HASH
#define EXTERNAL_KEYS_EQUAL BIG_KEYS_EQUAL

#include "../map.h"


#endif /* MAP_TEST_KEY_TEST_VALUE */
