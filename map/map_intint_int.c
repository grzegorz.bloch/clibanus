#include "map_intint_int.h"

#define MAP_KEY IntInt
#define MAP_VALUE int

#define EXTERNAL_KEY_HASH BIG_KEY_HASH
#define EXTERNAL_KEYS_EQUAL BIG_KEYS_EQUAL

#include "../map.c"
