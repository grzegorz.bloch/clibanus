#include "map_testkey_testvalue.h"

#define MAP_KEY TestKey
#define MAP_VALUE TestValue

#define KEY_BY_PTR
#define VALUE_BY_PTR

#define EXTERNAL_KEY_HASH BIG_KEY_HASH
#define EXTERNAL_KEYS_EQUAL BIG_KEYS_EQUAL

#include "../map.c"
