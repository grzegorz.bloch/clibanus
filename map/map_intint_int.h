#ifndef MAP_INTINT_INT
#define MAP_INTINT_INT


#include "../pair/int_int.h"
#define MAP_KEY IntInt
#define MAP_VALUE int

#define EXTERNAL_KEY_HASH BIG_KEY_HASH
#define EXTERNAL_KEYS_EQUAL BIG_KEYS_EQUAL

#include "../map.h"


#endif /* MAP_INTINT_INT */
