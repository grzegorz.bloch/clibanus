#include "logg.h"
#include "fmt.h"

#include <stdio.h>
#include <time.h>
#include <pthread.h>


static LoggLevel __level = INFO;

static LoggMode __mode = NORMAL;

static FILE *__fp = NULL;
static pthread_mutex_t __mutex = PTHREAD_MUTEX_INITIALIZER;


#define RETURN(result) { \
    pthread_mutex_unlock(&__mutex); \
    return (result); \
}


static const char *const __levelStr[] = {
    "TRACE",
    "DEBUG",
    "INFO ",
    "WARN ",
    "ERROR"
};


void setLoggFile(FILE *fp) {
    pthread_mutex_lock(&__mutex);
    __fp = fp;
    pthread_mutex_unlock(&__mutex);
}


void setLoggLevel(LoggLevel level) {
    pthread_mutex_lock(&__mutex);
    __level = level;
    pthread_mutex_unlock(&__mutex);
}


int isLogging(LoggLevel level) {
    pthread_mutex_lock(&__mutex);
    int isLogging = level >= __level;
    pthread_mutex_unlock(&__mutex);

    return isLogging;
}


void setLoggMode(LoggMode mode) {
    pthread_mutex_lock(&__mutex);
    __mode = mode;
    pthread_mutex_unlock(&__mutex);
}


static int logHeader(LoggLevel level) {
     if (__mode == VERBATIM || __mode == HEADLESS)
        return 0;

    struct timespec now;
    timespec_get(&now, TIME_UTC);
    char timeStr[32];
    strftime(timeStr, sizeof(timeStr), "%d/%m/%Y %T", gmtime(&now.tv_sec));

    pthread_t tid = pthread_self();
    char tidBuffer[128];
    sprinth(tidBuffer, &tid, sizeof(tid), sizeof(tid));

    return fprintf(
        __fp,
        "%s.%09ld [%s] %s - ",
        timeStr,
        now.tv_nsec,
        tidBuffer,
        __levelStr[level]
    );
}


static int vlogg(LoggLevel level, const char *restrict format, va_list argp) {
    if (level < __level)
        return 0;

    pthread_mutex_lock(&__mutex);

    int n = logHeader(level);
    if (n < 0)
        RETURN(-1);

    int err = vfprintf(__fp, format, argp);
    if (err < 0)
        RETURN(-1);
    n += err;

    if (__mode == VERBATIM)
        RETURN(n);

    err = fprintf(__fp, "\n");
    if (err < 0)
        RETURN(-1);
    n += err;

    RETURN(n);
}


#define LOGG(_level) { \
	va_list argp; \
	va_start(argp, format); \
	int r = vlogg((_level), format, argp); \
	va_end(argp); \
    return r; \
}


int logg(LoggLevel level, const char *restrict format, ...) {
    LOGG(level);
}

int loggt(const char *restrict format, ...) {
    LOGG(TRACE);
}

int loggd(const char *restrict format, ...) {
    LOGG(DEBUG);
}

int loggi(const char *restrict format, ...) {
    LOGG(INFO);
}

int loggw(const char *restrict format, ...) {
    LOGG(WARN);
}

int logge(const char *restrict format, ...) {
    LOGG(ERROR);
}


int loggh(
    LoggLevel level,
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    if (level < __level)
        return 0;

    pthread_mutex_lock(&__mutex);

    int n = logHeader(level);
    if (n < 0)
        RETURN(-1);

    int err = fprintf(__fp, format);
    if (err < 0)
        RETURN(-1);
    n += err;

    err = fprinth(__fp, bytes, numBytes, numSeparated);
    if (err < 0)
        RETURN(-1);
    n += err;

    if (__mode == VERBATIM)
        RETURN(n);

    err = fprintf(__fp, "\n");
    if (err < 0)
        RETURN(-1);
    n += err;

    RETURN(n);
}


int logght(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return loggh(TRACE, format, bytes, numBytes, numSeparated);
}

int logghd(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return loggh(DEBUG, format, bytes, numBytes, numSeparated);
}

int logghi(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return loggh(INFO, format, bytes, numBytes, numSeparated);
}

int logghw(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return loggh(WARN, format, bytes, numBytes, numSeparated);
}

int logghe(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return loggh(ERROR, format, bytes, numBytes, numSeparated);
}
