#include "queue_def.h"

#include <assert.h>
#include <string.h>
#include <stdlib.h>


#define INITIAL_QUEUE_CAPACITY 8


int INIT_QUEUE(
    QUEUE *q,
    size_t capacity
) {
    assert(capacity > 0);

    size_t itemsSize = sizeof(QUEUE_ITEM) * capacity;
    QUEUE_ITEM *items = (QUEUE_ITEM *) malloc(itemsSize);
    if (items == NULL) {
        memset(q, 0, sizeof(QUEUE));

        return -1;
    }

    q->items = items;
    q->capacity = capacity;
    q->minCapacity = capacity;
    q->size = 0;
    q->head = 0;
    q->tail = capacity - 1;

    return 0;
}


void RELEASE_QUEUE(QUEUE *q) {
    if (q->items != NULL) {
        free(q->items);
    }
    memset(q, 0, sizeof(QUEUE));
}


QUEUE MAKE_QUEUE(void) {
    QUEUE queue;
    (void) INIT_QUEUE(&queue, INITIAL_QUEUE_CAPACITY);
    return queue;
}


QUEUE MAKE_SIZED_QUEUE(size_t targetSize) {
    size_t capacity = targetSize << 1;
    QUEUE queue;
    (void) INIT_QUEUE(&queue, capacity);
    return queue;
}


QUEUE COPY_QUEUE(QUEUE *q) {
    QUEUE queue;
    memset(&queue, 0, sizeof(QUEUE));

    if (q->items == NULL)
        return queue;

    size_t itemsSize = sizeof(QUEUE_ITEM) * q->capacity;
    QUEUE_ITEM *items = (QUEUE_ITEM *) malloc(itemsSize);
    if (items == NULL)
        return queue;
    memcpy(items, q->items, itemsSize);

    queue = *q;
    queue.items = items;

    return queue;
}


#define RESIZE_QUEUE MAKE_TOKEN(resize, QUEUE)
static int RESIZE_QUEUE(QUEUE *q, size_t newCapacity) {
    assert(newCapacity >= q->minCapacity);

    size_t itemsSize = sizeof(QUEUE_ITEM) * newCapacity;
    QUEUE_ITEM *items = (QUEUE_ITEM *) malloc(itemsSize);
    if (items == NULL)
        return -1;

    if (q->head <= q->tail) {
        size_t copySize = sizeof(QUEUE_ITEM) * (q->tail - q->head + 1);
        memcpy(items, q->items + q->head, copySize);
    }
    else {
        size_t dh = q->capacity - q->head;
        size_t headCopySize = sizeof(QUEUE_ITEM) * dh;
        memcpy(items, q->items + q->head, headCopySize);

        size_t tailCopySize = sizeof(QUEUE_ITEM) * (q->tail + 1);
        memcpy(items + dh, q->items, tailCopySize);
    }

    free(q->items);

    q->items = items;
    q->capacity = newCapacity;
    q->head = 0;
    q->tail = q->size - 1;

    return 0;
}


#define ENSURE_QUEUE_BIG_ENOUGH() {\
    if (q->size >= q->capacity) \
        if (RESIZE_QUEUE(q, q->capacity << 1) != 0) \
            return NULL; \
}


QUEUE_ITEM *ADD_TO_QUEUE(QUEUE *q, QUEUE_ITEM item) {
    ENSURE_QUEUE_BIG_ENOUGH();

    q->tail++;
    q->tail %= q->capacity;
    q->size++;

    QUEUE_ITEM *pi = q->items + q->tail;
    *pi = item;

    return pi;
}


QUEUE_ITEM *ADD_BY_PTR_TO_QUEUE(QUEUE *q, QUEUE_ITEM *item) {
    ENSURE_QUEUE_BIG_ENOUGH();

    q->tail++;
    q->tail %= q->capacity;
    q->size++;

    QUEUE_ITEM *pi = q->items + q->tail;
    *pi = *item;

    return pi;
}


int REMOVE_FROM_QUEUE(QUEUE *q, QUEUE_ITEM *item) {
    if (q->size <= 0)
        return -1;

    if (item != NULL)
        memcpy(item, q->items + q->head, sizeof(QUEUE_ITEM));

    q->head++;
    q->head %= q->capacity;
    q->size--;

    size_t newCapacity = q->capacity >> 1;
    if (newCapacity >= q->minCapacity && q->size <= newCapacity >> 1)
        if (RESIZE_QUEUE(q, newCapacity) != 0)
            return -1;

    return 0;
}


QUEUE_ITEM GET_FROM_QUEUE(QUEUE *q) {
    return q->items[q->head];
}


QUEUE_ITEM *GET_PTR_FROM_QUEUE(QUEUE *q) {
    return q->items + q->head;
}


#undef RESIZE_QUEUE
#undef ENSURE_QUEUE_BIG_ENOUGH
#undef INITIAL_QUEUE_CAPACITY


#include "queue_undef.h"
