/**
 * All common Array un-definitions.
 */


#undef RESIZE_ARRAY
#undef REDUCE_ARRAY
#undef ADD_EMPTY_TO_ARRAY
#undef ADD_BY_PTR_TO_ARRAY
#undef ADD_TO_ARRAY
#undef COPY_ARRAY
#undef MAKE_SIZED_ARRAY
#undef MAKE_ARRAY
#undef RELEASE_ARRAY
#undef INIT_ARRAY

#undef ARRAY
#undef ARRAY_ITEM

#undef ARRAY_DEFINE_H
