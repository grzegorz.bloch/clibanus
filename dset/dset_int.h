#ifndef DSET_INT
#define DSET_INT


#include <stddef.h>


struct DsetItem_int {
    int value;

    int parent;
    size_t rank;
};
typedef struct DsetItem_int DsetItem_int;


#define MAP_KEY int
#define MAP_VALUE DsetItem_int
#define VALUE_BY_PTR
#include "../map.h"


/**
 * Disjoint-set forest of ints
 * implemented using "union by rank" and "path compression" heuristics.
 */
struct Dset_int {
    Map_int_DsetItem_int items;
};
typedef struct Dset_int Dset_int;


int initDset_int(Dset_int *ds, size_t targetSize);

void releaseDset_int(Dset_int *ds);

Dset_int makeDset_int();

Dset_int makeSizedDset_int(size_t targetSize);

Dset_int copyDset_int(Dset_int *ds);


DsetItem_int *addToDset_int(
    Dset_int *ds,
    int x
);

DsetItem_int *getFromDset_int(
    Dset_int *ds,
    int x
);

int removeFromDset_int(
    Dset_int *ds,
    int x
);

DsetItem_int *unionDset_int(
    Dset_int *ds,
    int x, int y
);


void printDset_int(Dset_int *ds);


#endif /* DSET_INT */

