#include "dset_int.h"


#define MAP_KEY int
#define MAP_VALUE DsetItem_int
#define VALUE_BY_PTR
#include "../map.c"


#include <stdio.h>


#define DEFAULT_DISJOINT_SETS_TARGET_SIZE 8


int initDset_int(Dset_int *ds, size_t targetSize) {
    if (initMap_int_DsetItem_int(&ds->items, targetSize) != 0)
        return -1;

    return 0;
}


void releaseDset_int(Dset_int *ds) {
    releaseMap_int_DsetItem_int(&ds->items);
}


Dset_int makeDset_int() {
    Dset_int disjointSets;
    initDset_int(&disjointSets, DEFAULT_DISJOINT_SETS_TARGET_SIZE);
    return disjointSets;
}


Dset_int makeSizedDset_int(size_t targetSize) {
    Dset_int disjointSets;
    initDset_int(&disjointSets, targetSize);
    return disjointSets;
}


Dset_int copyDset_int(Dset_int *ds) {
    Dset_int dset;
    dset.items = copyMap_int_DsetItem_int(&ds->items);
    return dset;
}


DsetItem_int *addToDset_int(
    Dset_int *ds,
    int x
) {
    DsetItem_int *item = getValuePtrFromMap_int_DsetItem_int(&ds->items, x);
    if (item != NULL)
        return item;

    DsetItem_int newItem = {
        .value = x,
        .parent = x,
        .rank = 0
    };
    MapItem_int_DsetItem_int *mapItem = addToMap_int_DsetItem_int(
         &ds->items,
         x,
         &newItem
    );
    if (mapItem == NULL)
        return NULL;

    return &mapItem->value;
}


DsetItem_int *getFromDset_int(
    Dset_int *ds,
    int x
) {
    DsetItem_int *item = getValuePtrFromMap_int_DsetItem_int(&ds->items, x);
    if (item == NULL)
        return NULL;

    DsetItem_int *root = item;
    while (root->value != root->parent)
        root = getValuePtrFromMap_int_DsetItem_int(&ds->items, root->parent);

    /* Path compression */
    for (
        DsetItem_int *n = item;
        n->value != n->parent;
        n = getValuePtrFromMap_int_DsetItem_int(&ds->items, n->parent)
    )
        n->parent = root->value;

    return root;
}


int removeFromDset_int(
    Dset_int *ds,
    int x
) {
    DsetItem_int *root = getFromDset_int(ds, x);
    if (root == NULL)
        return 1;

    DsetItem_int *item = getValuePtrFromMap_int_DsetItem_int(&ds->items, x);
    assert(item != NULL);

    DsetItem_int *newRoot = NULL;
    if (item->value == item->parent) {
        /* Find new root by rank. */
        IteratorMap_int_DsetItem_int it = getIteratorMap_int_DsetItem_int(&ds->items);
        for (
            MapItem_int_DsetItem_int *mi = nextMapItem_int_DsetItem_int(&it);
            mi != NULL;
            mi = nextMapItem_int_DsetItem_int(&it)
        ) {
            if (mi->value.parent == item->value) {
                if (newRoot == NULL || mi->value.rank > newRoot->rank)
                    newRoot = &mi->value;
            }
        }

    }
    else {
        newRoot = root;
    }

    if (newRoot != NULL) {
        IteratorMap_int_DsetItem_int it = getIteratorMap_int_DsetItem_int(&ds->items);
        for (
            MapItem_int_DsetItem_int *mi = nextMapItem_int_DsetItem_int(&it);
            mi != NULL;
            mi = nextMapItem_int_DsetItem_int(&it)
        )
            if (mi->value.parent == item->value)
                mi->value.parent = newRoot->value;
    }

    int r = removeFromMap_int_DsetItem_int(&ds->items, x);
    assert(r == 0);

    return 0;
}


DsetItem_int *unionDset_int(
    Dset_int *ds,
    int x, int y
) {
    DsetItem_int *rootx = getFromDset_int(ds, x);
    assert(rootx != NULL);
    DsetItem_int *rooty = getFromDset_int(ds, y);
    assert(rooty != NULL);

    if (rootx == rooty)
        return rootx;

    /* Union by rank */
    if (rooty->rank > rootx->rank) {
        rootx->parent = y;

        return rooty;
    }
    else {
        rooty->parent = x;
        if (rootx->rank == rooty->rank)
            rootx->rank++;

        return rootx;
    }
}


void printDset_int(Dset_int *ds) {
    IteratorMap_int_DsetItem_int it = getIteratorMap_int_DsetItem_int(&ds->items);
    int n = 0;
    for (
        MapItem_int_DsetItem_int *imapItem = nextMapItem_int_DsetItem_int(&it);
        imapItem != NULL;
        imapItem = nextMapItem_int_DsetItem_int(&it)
    ) {
        if (imapItem->value.value != imapItem->value.parent)
            continue;
        // printf("%2d: .%d: [", n++, imapItem->value.value);
        printf("%2d: [", n++);

        IteratorMap_int_DsetItem_int jt = getIteratorMap_int_DsetItem_int(&ds->items);
        for (
            MapItem_int_DsetItem_int *jmapItem = nextMapItem_int_DsetItem_int(&jt);
            jmapItem != NULL;
            jmapItem = nextMapItem_int_DsetItem_int(&jt)
        ) {
            int s = jmapItem->key;
            DsetItem_int *root = getFromDset_int(ds, s);
            if (root == &imapItem->value)
                printf(".%d ", s);
        }
        printf("]\n");
    }
}

