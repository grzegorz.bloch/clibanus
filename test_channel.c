#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "channel/channel_int.h"


void testInitChannel(void **state) {
    Channel_int ch;
    initChannel_int(&ch, 54);
    assert_int_equal(ch.size, 0);
    assert_int_equal(ch.capacity, 54);
    assert_int_equal(ch.head, 0);
    assert_int_equal(ch.tail, 53);
    assert_non_null(ch.items);

    releaseChannel_int(&ch);
}


void testReleaseChannel(void **state) {
    Channel_int ch;
    initChannel_int(&ch, 7);
    releaseChannel_int(&ch);

    assert_int_equal(ch.size, 0);
    assert_int_equal(ch.capacity, 0);
    assert_int_equal(ch.head, 0);
    assert_int_equal(ch.tail, 0);
    assert_null(ch.items);
}


void testMakeChannel(void **state) {
    Channel_int ch = makeChannel_int(54);
    assert_int_equal(ch.size, 0);
    assert_int_equal(ch.capacity, 54);
    assert_int_equal(ch.head, 0);
    assert_int_equal(ch.tail, 53);
    assert_non_null(ch.items);

    releaseChannel_int(&ch);
}


void testAddToChannel(void **state) {
    Channel_int ch = makeChannel_int(128);
    assert_int_equal(ch.size, 0);

    int n = 1;
    for (int i = 0; i < 128; ++i, ++n) {
        addToChannel_int(&ch, n);
        int *pi = ch.items + ch.size - 1;
        assert_int_equal(*pi, n);
        assert_int_equal(ch.capacity, 128);
        assert_int_equal(ch.size, n);
        assert_int_equal(ch.head, 0);
        assert_int_equal(ch.tail, n - 1);
    }

    for (int i = 0; i < 128; ++i)
        assert_int_equal(ch.items[i], i + 1);

    releaseChannel_int(&ch);
}


void testAddByPtrToChannel(void **state) {
    Channel_int ch = makeChannel_int(128);
    assert_int_equal(ch.size, 0);

    int n = 1;
    for (int i = 0; i < 128; ++i, ++n) {
        addByPtrToChannel_int(&ch, &n);
        int *pi = ch.items + ch.size - 1;
        assert_int_equal(*pi, n);
        assert_int_equal(ch.capacity, 128);
        assert_int_equal(ch.size, n);
        assert_int_equal(ch.head, 0);
        assert_int_equal(ch.tail, n - 1);
    }

    for (int i = 0; i < 128; ++i)
        assert_int_equal(ch.items[i], i + 1);

    releaseChannel_int(&ch);
}


void testGetFromChannel(void **state) {
    Channel_int ch = makeChannel_int(128);
    assert_int_equal(ch.size, 0);

    for (int i = 0; i < 128; ++i)
        addToChannel_int(&ch, i + 1);

    for (int i = 0; i < 128; ++i) {
        int n = getFromChannel_int(&ch);
        assert_int_equal(n, i + 1);
        assert_int_equal(ch.capacity, 128);
        assert_int_equal(ch.size, 128 - n);
        assert_int_equal(ch.head, n % 128);
        assert_int_equal(ch.tail, 127);
    }

    releaseChannel_int(&ch);
}
