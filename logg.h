#ifndef LOGG_H
#define LOGG_H


#include <stdarg.h>
#include <stdio.h>


enum LoggLevel {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR
};
typedef enum LoggLevel LoggLevel;


enum LoggMode {
    NORMAL,
    HEADLESS,
    VERBATIM
};
typedef enum LoggMode LoggMode;


void setLoggFile(FILE *fp);

void setLoggLevel(LoggLevel level);

int isLogging(LoggLevel level);


void setLoggMode(LoggMode mode);


int logg(LoggLevel level, const char *restrict format, ...);

int loggt(const char *restrict format, ...);

int loggd(const char *restrict format, ...);

int loggi(const char *restrict format, ...);

int loggw(const char *restrict format, ...);

int logge(const char *restrict format, ...);


int loggh(
    LoggLevel level,
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
);

int logght(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
);

int logghd(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
);

int logghi(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
);

int logghw(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
);

int logghe(
    const char *restrict format,
    void *bytes,
    int numBytes,
    int numSeparated
);


#endif // LOGG_H
