#include "map/map_int_char.h"
#include "map/map_testkey_testvalue.h"

#include <stddef.h>
#include <string.h>

#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>



int findCapacityIndexOfMap_int_char(size_t capacity);

void testFindMapCapacityIndex(void **state) {
    assert_int_equal(findCapacityIndexOfMap_int_char(0), 4);
    assert_int_equal(findCapacityIndexOfMap_int_char(1), 4);

    assert_int_equal(findCapacityIndexOfMap_int_char(12), 4);
    assert_int_equal(findCapacityIndexOfMap_int_char(13), 4);
    assert_int_equal(findCapacityIndexOfMap_int_char(14), 5);
    assert_int_equal(findCapacityIndexOfMap_int_char(15), 5);

    assert_int_equal(findCapacityIndexOfMap_int_char(29), 5);
    assert_int_equal(findCapacityIndexOfMap_int_char(30), 5);
    assert_int_equal(findCapacityIndexOfMap_int_char(31), 5);
    assert_int_equal(findCapacityIndexOfMap_int_char(32), 6);
    assert_int_equal(findCapacityIndexOfMap_int_char(33), 6);

    assert_int_equal(findCapacityIndexOfMap_int_char(50), 6);

    assert_int_equal(findCapacityIndexOfMap_int_char(59), 6);
    assert_int_equal(findCapacityIndexOfMap_int_char(60), 6);
    assert_int_equal(findCapacityIndexOfMap_int_char(61), 6);
    assert_int_equal(findCapacityIndexOfMap_int_char(62), 7);
    assert_int_equal(findCapacityIndexOfMap_int_char(63), 7);

    assert_int_equal(findCapacityIndexOfMap_int_char(4503599627370447), 52);
    assert_int_equal(findCapacityIndexOfMap_int_char(4503599627370448), 52);
    assert_int_equal(findCapacityIndexOfMap_int_char(4503599627370449), 52);
    assert_int_equal(findCapacityIndexOfMap_int_char(4503599627370450), 53);
    assert_int_equal(findCapacityIndexOfMap_int_char(4503599627370451), 53);

    assert_int_equal(findCapacityIndexOfMap_int_char(9223372036854775781), 63);
    assert_int_equal(findCapacityIndexOfMap_int_char(9223372036854775782), 63);
    assert_int_equal(findCapacityIndexOfMap_int_char(9223372036854775783), 63);
    assert_int_equal(findCapacityIndexOfMap_int_char(9223372036854775784), 63);
    assert_int_equal(findCapacityIndexOfMap_int_char(9223372036854775785), 63);
}

void testInitMap(void **state) {
    Map_int_char m;
    initMap_int_char(&m, 156);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacityIndex, 9);
    assert_non_null(m.items);

    releaseMap_int_char(&m);
}


void testReleaseMap(void **state) {
    Map_int_char m;
    initMap_int_char(&m, 1234);

    releaseMap_int_char(&m);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacityIndex, 0);
    assert_null(m.items);
}


void testMakeMap(void **state) {
    Map_int_char m = makeMap_int_char();
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacityIndex, 4);
    assert_non_null(m.items);

    releaseMap_int_char(&m);
}


void testMakeSizedMap(void **state) {
    Map_int_char m = makeSizedMap_int_char(10000);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacityIndex, 15);
    assert_int_equal(m.capacity, 32749);
    assert_non_null(m.items);

    releaseMap_int_char(&m);
}


void testCopyMap(void **state) {
    Map_int_char m = makeMap_int_char();
    for (int i = 0; i < 1000; ++i)
        addToMap_int_char(&m, i, i % 128);

    assert_int_equal(m.size, 1000);
    assert_int_equal(m.capacity, 2039);
    assert_int_equal(m.capacityIndex, 11);
    assert_non_null(m.items);


    Map_int_char n = copyMap_int_char(&m);
    assert_int_equal(n.size, 1000);
    assert_int_equal(n.capacity, 2039);
    assert_int_equal(n.capacityIndex, 11);
    assert_non_null(n.items);

    assert_ptr_not_equal(n.items, m.items);
    assert_memory_equal(n.items, m.items, sizeof(MapItem_int_char) * 2039);

    releaseMap_int_char(&n);
    releaseMap_int_char(&m);
}


void testAddToMapExtends(void **state) {
    Map_int_char m = makeMap_int_char();

    MapItem_int_char* pi = addToMap_int_char(&m, 123, 'a');
    assert_non_null(pi);
    assert_int_equal(pi->key, 123);
    assert_int_equal(pi->value, 'a');
    assert_int_equal(pi->isPresent, 1);
    assert_int_equal(m.size, 1);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(m.capacityIndex, 4);

    assert_int_equal(getValueFromMap_int_char(&m, 123), 'a');
    char *pc = getValuePtrFromMap_int_char(&m, 124);
    assert_null(pc);

    for (int i = 0; i < 5; ++i)
        addToMap_int_char(&m, i + 1, i);

    assert_int_equal(m.size, 6);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(m.capacityIndex, 4);

    pi = addToMap_int_char(&m, 123123, 'b');
    assert_non_null(pi);
    assert_int_equal(pi->key, 123123);
    assert_int_equal(pi->value, 'b');
    assert_int_equal(pi->isPresent, 1);
    assert_int_equal(m.size, 7);
    assert_int_equal(m.capacity, 31);
    assert_int_equal(m.capacityIndex, 5);
    assert_int_equal(getValueFromMap_int_char(&m, 123), 'a');
    assert_int_equal(getValueFromMap_int_char(&m, 123123), 'b');

    for (int i = 0; i < 5; ++i)
        addToMap_int_char(&m, 1000000 * i, i);

    assert_int_equal(m.size, 12);
    assert_int_equal(m.capacity, 31);
    assert_int_equal(m.capacityIndex, 5);

    pi = addToMap_int_char(&m, 123123123, 'c');
    assert_non_null(pi);
    assert_int_equal(pi->key, 123123123);
    assert_int_equal(pi->value, 'c');
    assert_int_equal(pi->isPresent, 1);
    assert_int_equal(m.size, 13);
    assert_int_equal(m.capacity, 61);
    assert_int_equal(m.capacityIndex, 6);
    assert_int_equal(getValueFromMap_int_char(&m, 123), 'a');
    assert_int_equal(getValueFromMap_int_char(&m, 123123), 'b');
    assert_int_equal(getValueFromMap_int_char(&m, 123123123), 'c');

    releaseMap_int_char(&m);
}


void testRemoveFromMapReduces(void **state) {
    Map_int_char m = makeMap_int_char();

    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(m.capacityIndex, 4);

    for (int i = 0; i < 100; ++i)
        assert_non_null(addToMap_int_char(&m, i, (i + 1) % 128));

    assert_int_equal(m.size, 100);
    assert_int_equal(m.capacity, 251);
    assert_int_equal(m.capacityIndex, 8);

    for (int i = 100; i > 31; --i) {
        assert_int_equal(m.size, i);
        assert_int_equal(m.capacity, 251);
        assert_int_equal(m.capacityIndex, 8);

        assert_non_null(getItemFromMap_int_char(&m, i - 1));
        assert_non_null(getValuePtrFromMap_int_char(&m, i - 1));
        assert_int_equal(getValueFromMap_int_char(&m, i - 1), i % 128);

        assert_int_equal(removeFromMap_int_char(&m, i - 1), 0);

        assert_null(getItemFromMap_int_char(&m, i - 1));
        assert_null(getValuePtrFromMap_int_char(&m, i - 1));

        assert_int_equal(m.size, i - 1);
        assert_int_equal(m.capacity, 251);
        assert_int_equal(m.capacityIndex, 8);
    }

    assert_int_equal(m.size, 31);
    assert_int_equal(m.capacity, 251);
    assert_int_equal(m.capacityIndex, 8);

    assert_int_equal(removeFromMap_int_char(&m, 30), 0);

    assert_int_equal(m.size, 30);
    assert_int_equal(m.capacity, 113);
    assert_int_equal(m.capacityIndex, 7);

    for (int i = 30; i > 13; --i) {
        assert_int_equal(m.size, i);
        assert_int_equal(m.capacity, 113);
        assert_int_equal(m.capacityIndex, 7);

        assert_non_null(getItemFromMap_int_char(&m, i - 1));
        assert_non_null(getValuePtrFromMap_int_char(&m, i - 1));
        assert_int_equal(getValueFromMap_int_char(&m, i - 1), i % 128);

        assert_int_equal(removeFromMap_int_char(&m, i - 1), 0);

        assert_null(getItemFromMap_int_char(&m, i - 1));
        assert_null(getValuePtrFromMap_int_char(&m, i - 1));

        assert_int_equal(m.size, i - 1);
        assert_int_equal(m.capacity, 113);
        assert_int_equal(m.capacityIndex, 7);
    }

    assert_int_equal(m.size, 13);
    assert_int_equal(m.capacity, 113);
    assert_int_equal(m.capacityIndex, 7);

    assert_int_equal(removeFromMap_int_char(&m, 12), 0);

    assert_int_equal(m.size, 12);
    assert_int_equal(m.capacity, 61);
    assert_int_equal(m.capacityIndex, 6);

    for (int i = 12; i > 7; --i) {
        assert_int_equal(m.size, i);
        assert_int_equal(m.capacity, 61);
        assert_int_equal(m.capacityIndex, 6);

        assert_non_null(getItemFromMap_int_char(&m, i - 1));
        assert_non_null(getValuePtrFromMap_int_char(&m, i - 1));
        assert_int_equal(getValueFromMap_int_char(&m, i - 1), i % 128);

        assert_int_equal(removeFromMap_int_char(&m, i - 1), 0);

        assert_null(getItemFromMap_int_char(&m, i - 1));
        assert_null(getValuePtrFromMap_int_char(&m, i - 1));

        assert_int_equal(m.size, i - 1);
        assert_int_equal(m.capacity, 61);
        assert_int_equal(m.capacityIndex, 6);
    }

    assert_int_equal(m.size, 7);
    assert_int_equal(m.capacity, 61);
    assert_int_equal(m.capacityIndex, 6);

    assert_int_equal(removeFromMap_int_char(&m, 6), 0);

    assert_int_equal(m.size, 6);
    assert_int_equal(m.capacity, 31);
    assert_int_equal(m.capacityIndex, 5);

    for (int i = 6; i > 3; --i) {
        assert_int_equal(m.size, i);
        assert_int_equal(m.capacity, 31);
        assert_int_equal(m.capacityIndex, 5);

        assert_non_null(getItemFromMap_int_char(&m, i - 1));
        assert_non_null(getValuePtrFromMap_int_char(&m, i - 1));
        assert_int_equal(getValueFromMap_int_char(&m, i - 1), i % 128);

        assert_int_equal(removeFromMap_int_char(&m, i - 1), 0);

        assert_null(getItemFromMap_int_char(&m, i - 1));
        assert_null(getValuePtrFromMap_int_char(&m, i - 1));

        assert_int_equal(m.size, i - 1);
        assert_int_equal(m.capacity, 31);
        assert_int_equal(m.capacityIndex, 5);
    }

    assert_int_equal(m.size, 3);
    assert_int_equal(m.capacity, 31);
    assert_int_equal(m.capacityIndex, 5);

    assert_int_equal(removeFromMap_int_char(&m, 2), 0);

    assert_int_equal(m.size, 2);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(m.capacityIndex, 4);

    assert_int_equal(removeFromMap_int_char(&m, 1), 0);

    assert_int_equal(m.size, 1);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(m.capacityIndex, 4);

    assert_int_equal(removeFromMap_int_char(&m, 0), 0);

    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(m.capacityIndex, 4);

    releaseMap_int_char(&m);
}


#define KEY_HASH(key, capacity) (((size_t) (key)) % (capacity))


void testAddToMapCollision(void **state) {
    Map_int_char m = makeSizedMap_int_char(100);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacity, 251);
    assert_int_equal(m.capacityIndex, 8);

    /* Continuous collisions */
    MapItem_int_char *pi10 =  addToMap_int_char(&m, 10, 'a');
    assert_non_null(pi10);
    assert_int_equal(m.capacity, 251);
    int hash10 = KEY_HASH(10, m.capacity);

    MapItem_int_char *pi261 = addToMap_int_char(&m, 261, 'b');
    assert_non_null(pi261);
    assert_int_equal(m.capacity, 251);
    int hash261 = KEY_HASH(261, m.capacity);

    MapItem_int_char *pi512 = addToMap_int_char(&m, 512, 'c');
    assert_non_null(pi512);
    assert_int_equal(m.capacity, 251);
    int hash512 = KEY_HASH(512, m.capacity);
    assert_int_equal(hash10, hash261);
    assert_int_equal(hash261, hash512);
    assert_ptr_equal(pi10 + 1, pi261);
    assert_ptr_equal(pi261 + 1, pi512);

    assert_int_equal(getValueFromMap_int_char(&m, 10), 'a');
    assert_int_equal(getValueFromMap_int_char(&m, 261), 'b');
    assert_int_equal(getValueFromMap_int_char(&m, 512), 'c');

    /* Discontinuous collisions */
    MapItem_int_char *pi249 =  addToMap_int_char(&m, 249, 'u');
    assert_non_null(pi249);
    assert_int_equal(m.capacity, 251);
    int hash249 = KEY_HASH(249, m.capacity);

    MapItem_int_char *pi500 = addToMap_int_char(&m, 500, 'w');
    assert_non_null(pi500);
    assert_int_equal(m.capacity, 251);
    int hash500 = KEY_HASH(500, m.capacity);

    MapItem_int_char *pi751 = addToMap_int_char(&m, 751, 'x');
    assert_non_null(pi751);
    assert_int_equal(m.capacity, 251);
    int hash751 = KEY_HASH(751, m.capacity);

    MapItem_int_char *pi1002 = addToMap_int_char(&m, 1002, 'y');
    assert_non_null(pi1002);
    assert_int_equal(m.capacity, 251);
    int hash1002 = KEY_HASH(1002, m.capacity);

    assert_int_equal(getValueFromMap_int_char(&m, 249), 'u');
    assert_int_equal(getValueFromMap_int_char(&m, 500), 'w');
    assert_int_equal(getValueFromMap_int_char(&m, 751), 'x');
    assert_int_equal(getValueFromMap_int_char(&m, 1002), 'y');

    assert_int_equal(hash249, hash500);
    assert_int_equal(hash500, hash751);
    assert_int_equal(hash751, hash1002);
    assert_ptr_equal(pi249 + 1, pi500);
    assert_ptr_equal(pi500, m.items + m.capacity - 1);
    assert_ptr_equal(pi751, m.items);
    assert_ptr_equal(pi500, pi751 + m.capacity - 1);
    assert_ptr_equal(pi751 + 1, pi1002);


    releaseMap_int_char(&m);
}


void testRemoveFromMapCluster(void **state) {
    Map_int_char m = makeSizedMap_int_char(100);
    assert_int_equal(m.capacity, 251);

    /* Presence of these items will prevent the map from shrinking. */
    for (int i = 0; i < 50; ++i)
        assert_non_null(addToMap_int_char(&m, i + 100, i));

    /*
    printHexNum(
        m.items,
        sizeof(MapItem_int_char) * m.capacity,
        sizeof(MapItem_int_char)
    );
    */

    /* Discontinuous cluster */
    assert_non_null(addToMap_int_char(&m, 249, 1));
    assert_non_null(addToMap_int_char(&m, 500, 2));
    assert_non_null(addToMap_int_char(&m, 751, 3));
    assert_non_null(addToMap_int_char(&m, 1002, 4));

    MapItem_int_char *pi249 =  getItemFromMap_int_char(&m, 249);
    MapItem_int_char *pi500 =  getItemFromMap_int_char(&m, 500);
    MapItem_int_char *pi751 =  getItemFromMap_int_char(&m, 751);
    MapItem_int_char *pi1002 =  getItemFromMap_int_char(&m, 1002);

    assert_non_null(pi249);
    assert_ptr_equal(pi249, m.items + m.capacity - 2);
    assert_non_null(pi500);
    assert_ptr_equal(pi500, m.items + m.capacity - 1);
    assert_ptr_equal(pi500, pi249 + 1);
    assert_non_null(pi751);
    assert_ptr_equal(pi751, m.items);
    assert_non_null(pi1002);
    assert_ptr_equal(pi751 + 1, pi1002);

    /* Removed first from cluster */
    assert_int_equal(removeFromMap_int_char(&m, 249), 0);
    pi249 = getItemFromMap_int_char(&m, 249);
    pi500 = getItemFromMap_int_char(&m, 500);
    pi751 = getItemFromMap_int_char(&m, 751);
    pi1002 = getItemFromMap_int_char(&m, 1002);
    assert_null(pi249);
    assert_non_null(pi500);
    assert_ptr_equal(pi500, m.items + m.capacity - 2);
    assert_non_null(pi751);
    assert_ptr_equal(pi751, m.items + m.capacity - 1);
    assert_ptr_equal(pi751, pi500 + 1);
    assert_non_null(pi1002);
    assert_ptr_equal(pi1002, m.items);

    assert_non_null(addToMap_int_char(&m, 249, 1));
    /* Removed second from cluster */
    assert_int_equal(removeFromMap_int_char(&m, 751), 0);
    pi249 = getItemFromMap_int_char(&m, 249);
    pi500 = getItemFromMap_int_char(&m, 500);
    pi751 = getItemFromMap_int_char(&m, 751);
    pi1002 = getItemFromMap_int_char(&m, 1002);
    assert_non_null(pi249);
    assert_ptr_equal(pi249, m.items);
    assert_non_null(pi500);
    assert_ptr_equal(pi500, m.items + m.capacity - 2);
    assert_null(pi751);
    assert_non_null(pi1002);
    assert_ptr_equal(pi1002, m.items + m.capacity - 1);

    assert_non_null(addToMap_int_char(&m, 751, 3));
    /* Removed third from cluster */
    assert_int_equal(removeFromMap_int_char(&m, 249), 0);
    pi249 = getItemFromMap_int_char(&m, 249);
    pi500 = getItemFromMap_int_char(&m, 500);
    pi751 = getItemFromMap_int_char(&m, 751);
    pi1002 = getItemFromMap_int_char(&m, 1002);
    assert_null(pi249);
    assert_non_null(pi500);
    assert_ptr_equal(pi500, m.items + m.capacity - 2);
    assert_non_null(pi751);
    assert_ptr_equal(pi751, m.items);
    assert_non_null(pi1002);
    assert_ptr_equal(pi1002, m.items + m.capacity - 1);

    assert_non_null(addToMap_int_char(&m, 249, 1));
    /* Removed fourth from cluster */
    assert_int_equal(removeFromMap_int_char(&m, 249), 0);
    pi249 = getItemFromMap_int_char(&m, 249);
    pi500 = getItemFromMap_int_char(&m, 500);
    pi751 = getItemFromMap_int_char(&m, 751);
    pi1002 = getItemFromMap_int_char(&m, 1002);
    assert_null(pi249);
    assert_non_null(pi500);
    assert_ptr_equal(pi500, m.items + m.capacity - 2);
    assert_non_null(pi751);
    assert_ptr_equal(pi751, m.items);
    assert_non_null(pi1002);
    assert_ptr_equal(pi1002, m.items + m.capacity - 1);


    releaseMap_int_char(&m);
}


void testGetFromMap(void **state) {
    Map_int_char m = makeSizedMap_int_char(100);

    int k = 1;
    for (int i = 0; i < 18; ++i, k *= 10)
        assert_non_null(addToMap_int_char(&m, k, i + 1));

    k = 1;
    for (int i = 0; i < 18; ++i, k *= 10) {
        MapItem_int_char *item = getItemFromMap_int_char(&m, k);
        assert_non_null(item);
        assert_int_equal(item->key, k);
        assert_int_equal(item->value, i + 1);
        assert_int_equal(item->isPresent, 1);
        char value = getValueFromMap_int_char(&m, k);
        assert_int_equal(value, i + 1);
        char *valuePtr = getValuePtrFromMap_int_char(&m, k);
        assert_non_null(valuePtr);
        assert_int_equal(*valuePtr, i + 1);
    }

    releaseMap_int_char(&m);
}


void testMapIterator(void **state) {
    Map_int_char m = makeSizedMap_int_char(100);

    int k = 1;
    for (int i = 0; i < 9; ++i, k *= 10)
        assert_non_null(addToMap_int_char(&m, k, i + 1));

    IteratorMap_int_char it = getIteratorMap_int_char(&m);

    MapItem_int_char *item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 1);
    assert_int_equal(item->key, 1);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 2);
    assert_int_equal(item->key, 10);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 7);
    assert_int_equal(item->key, 1000000);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 9);
    assert_int_equal(item->key, 100000000);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 3);
    assert_int_equal(item->key, 100);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 6);
    assert_int_equal(item->key, 100000);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 8);
    assert_int_equal(item->key, 10000000);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 5);
    assert_int_equal(item->key, 10000);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_non_null(item);
    assert_int_equal(item->value, 4);
    assert_int_equal(item->key, 1000);
    assert_int_equal(item->isPresent, 1);

    item = nextMapItem_int_char(&it);
    assert_null(item);

    releaseMap_int_char(&m);
}


void testKeyByPtr(void **state) {
    Map_TestKey_TestValue m = makeMap_TestKey_TestValue();

    TestKey key;
    memset(&key, 0, sizeof(key));
    key = (TestKey) {5l, 123.8};

    TestValue value;
    memset(&value, 0, sizeof(value));
    value = (TestValue) {'v', 234};

    MapItem_TestKey_TestValue *pi = addToMap_TestKey_TestValue(&m, &key, &value);
    assert_non_null(pi);
    assert_int_equal(pi->key.l, 5l);
    assert_float_equal(pi->key.f, 123.8, 1e-8);
    assert_int_equal(pi->value.c, 'v');
    assert_int_equal(pi->value.i, 234);

    TestValue *pv = getValuePtrFromMap_TestKey_TestValue(&m, &key);
    assert_non_null(pv);
    assert_int_equal(pv->c, 'v');
    assert_int_equal(pv->i, 234);

    TestValue v = getValueFromMap_TestKey_TestValue(&m, &key);
    assert_int_equal(v.c, 'v');
    assert_int_equal(v.i, 234);

    pi = getItemFromMap_TestKey_TestValue(&m, &key);
    assert_non_null(pi);
    assert_int_equal(pi->key.l, 5l);
    assert_float_equal(pi->key.f, 123.8, 1e-8);
    assert_int_equal(pi->value.c, 'v');
    assert_int_equal(pi->value.i, 234);

    assert_int_equal(removeFromMap_TestKey_TestValue(&m, &key), 0);
    pi = getItemFromMap_TestKey_TestValue(&m, &key);
    assert_null(pi);

    releaseMap_TestKey_TestValue(&m);
}


void testRemoveMapItemAndReduceMap(void **state) {
    Map_int_char m = makeMap_int_char();

    for (int i = 0; i < 1021; ++i)
        assert_non_null(addToMap_int_char(&m, i, i % 256));
    assert_int_equal(m.size, 1021);
    assert_int_equal(m.capacity, 4093);

    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 1021);
    assert_int_equal(m.capacity, 4093);

    IteratorMap_int_char it = getIteratorMap_int_char(&m);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_int_equal(m.size, 1020);
    assert_int_equal(m.capacity, 4093);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 1020);
    assert_int_equal(m.capacity, 4093);

    it = getIteratorMap_int_char(&m);
    for (int i = 1020; i > 509; --i) {
        assert_non_null(nextMapItem_int_char(&it));
        removeMapItem_int_char(&m, &it);
    }
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 509);
    assert_int_equal(m.capacity, 4093);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_int_equal(m.size, 508);
    assert_int_equal(m.capacity, 4093);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 508);
    assert_int_equal(m.capacity, 2039);

    it = getIteratorMap_int_char(&m);
    for (int i = 508; i > 251; --i) {
        assert_non_null(nextMapItem_int_char(&it));
        removeMapItem_int_char(&m, &it);
    }
    assert_int_equal(m.size, 251);
    assert_int_equal(m.capacity, 2039);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 251);
    assert_int_equal(m.capacity, 2039);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_int_equal(m.size, 250);
    assert_int_equal(m.capacity, 2039);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 250);
    assert_int_equal(m.capacity, 1021);

    it = getIteratorMap_int_char(&m);
    for (int i = 250; i > 113; --i) {
        assert_non_null(nextMapItem_int_char(&it));
        removeMapItem_int_char(&m, &it);
    }
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 113);
    assert_int_equal(m.capacity, 1021);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_int_equal(m.size, 112);
    assert_int_equal(m.capacity, 1021);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 112);
    assert_int_equal(m.capacity, 509);


    it = getIteratorMap_int_char(&m);
    for (int i = 112; i > 3; --i) {
        assert_non_null(nextMapItem_int_char(&it));
        removeMapItem_int_char(&m, &it);
    }
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 3);
    assert_int_equal(m.capacity, 31);
    it = getIteratorMap_int_char(&m);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_non_null(nextMapItem_int_char(&it));
    removeMapItem_int_char(&m, &it);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacity, 31);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacity, 13);
    assert_int_equal(reduceMap_int_char(&m), 0);
    assert_int_equal(m.size, 0);
    assert_int_equal(m.capacity, 13);

    releaseMap_int_char(&m);
}
