FROM gcc:13.2.0

RUN apt-get update \
&& apt-get install -y libcmocka0 libcmocka-dev \
&& rm -rf /var/lib/apt/lists/*

COPY . /usr/src/clibanus
WORKDIR /usr/src/clibanus
