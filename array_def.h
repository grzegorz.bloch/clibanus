#ifndef ARRAY_DEFINE_H
#define ARRAY_DEFINE_H

/**
 * All common Array definitions
 */


#include "token.h"


#ifndef ARRAY
#define ARRAY MAKE_SEPARATED_TOKEN(Array, ARRAY_ITEM)
#endif /* ARRAY */


#define INIT_ARRAY MAKE_TOKEN(init, ARRAY)

#define RELEASE_ARRAY MAKE_TOKEN(release, ARRAY)

#define MAKE_ARRAY MAKE_TOKEN(make, ARRAY)

#define MAKE_SIZED_ARRAY MAKE_TOKEN(makeSized, ARRAY)

#define COPY_ARRAY MAKE_TOKEN(copy, ARRAY)

#define ADD_TO_ARRAY MAKE_TOKEN(addTo, ARRAY)

#define ADD_BY_PTR_TO_ARRAY MAKE_TOKEN(addByPtrTo, ARRAY)

#define ADD_EMPTY_TO_ARRAY MAKE_TOKEN(addEmptyTo, ARRAY)

#define REDUCE_ARRAY MAKE_TOKEN(reduce, ARRAY)

#define RESIZE_ARRAY MAKE_TOKEN(resize, ARRAY)


#endif /* ARRAY_DEFINE_H */
