#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "map_def.h"


#define MIN_MAP_CAPACITY_INDEX 4
#define MAX_MAP_CAPACITY_INDEX 63


/**
 * Map capacities are maximal primes equal to  2^i - k,
 * where i is array index and k >= 0.
 */
static size_t capacities[MAX_MAP_CAPACITY_INDEX + 1] = {
    1, 2, 3, 7,
    13, 31, 61, 113,
    251, 509, 1021, 2039,
    4093, 8191, 16381, 32749,
    65521, 131071, 262139, 524287,
    1048573, 2097143, 4194301, 8388593,
    16777213, 33554393, 67108859, 134217689,
    268435399, 536870909, 1073741789, 2147483647,
    4294967291, 8589934583, 17179869143, 34359738337,
    68719476731, 137438953447, 274877906899, 549755813881,
    1099511627689, 2199023255531, 4398046511093, 8796093022151,
    17592186044399, 35184372088777, 70368744177643, 140737488355213,
    281474976710597, 562949953421231, 1125899906842597, 2251799813685119,
    4503599627370449, 9007199254740881, 18014398509481951, 36028797018963913,
    72057594037927931, 144115188075855859, 288230376151711717, 576460752303423433,
    1152921504606846883, 2305843009213693951, 4611686018427387847, 9223372036854775783
};


#ifdef KEY_BY_PTR
size_t BIG_KEY_HASH(MAP_KEY *key, size_t capacity) {
    uint8_t *k = (uint8_t *) key;
#else
size_t BIG_KEY_HASH(MAP_KEY key, size_t capacity) {
    uint8_t *k = (uint8_t *) &key;
#endif
    size_t h = 0;
    for (size_t i = 0; i < sizeof(MAP_KEY); ++i)
        h = (255 * h + *k++) % capacity;
    return h;
}


#define FIND_CAPACITY_INDEX MAKE_TOKEN(findCapacityIndexOf, MAP)
int FIND_CAPACITY_INDEX(size_t capacity) {
    if (capacity <= capacities[MIN_MAP_CAPACITY_INDEX])
        return MIN_MAP_CAPACITY_INDEX;

    if (capacity > capacities[MAX_MAP_CAPACITY_INDEX - 1])
        return MAX_MAP_CAPACITY_INDEX;

    int l = MIN_MAP_CAPACITY_INDEX + 1;
    int r = MAX_MAP_CAPACITY_INDEX - 1;
    for ( ; ; ) {
        int m = (l + r) >> 1;
        if (capacity > capacities[m]) {
            if (capacity > capacities[m + 1])
                l = m + 1;
            else
                return m + 1;
        }
        else if (capacity < capacities[m]) {
            if (capacity < capacities[m - 1])
                r = m - 1;
            else if (capacity > capacities[m - 1])
                return m;
            else
                return m - 1;
        }
        else {
            return m;
        }
    }
}


int INIT(MAP *m, size_t targetSize) {
    size_t capacity = targetSize << 1;
    int capacityIndex = FIND_CAPACITY_INDEX(capacity);
    capacity = capacities[capacityIndex];

    size_t itemsSize = sizeof(MAP_ITEM) * capacity;
    MAP_ITEM *items = (MAP_ITEM *) malloc(itemsSize);
    if (items == NULL) {
        m->items = NULL;
        m->size = 0;
        m->capacity = 0;
        m->capacityIndex = 0;

        return -1;
    }

    /* Implicitly set all items not present. */
    memset(items, 0, itemsSize);

    m->items = items;
    m->size = 0;
    m->capacity = capacity;
    m->capacityIndex = capacityIndex;

    return 0;
}


void RELEASE(MAP *m) {
    if (m->items != NULL) {
        free(m->items);
        m->items = NULL;
    }
    m->size = 0;
    m->capacity = 0;
    m->capacityIndex = 0;
}


MAP MAKE(void) {
    MAP map;
    (void) INIT(&map, 0);
    return map;
}


MAP MAKE_SIZED(size_t targetSize) {
    MAP map;
    (void) INIT(&map, targetSize);
    return map;
}


MAP COPY(MAP *m) {
    MAP map = {
        .items = NULL,
        .size = 0,
        .capacity = 0,
        .capacityIndex = 0
    };

    if (m->items == NULL)
        return map;

    size_t itemsSize = sizeof(MAP_ITEM) * m->capacity;
    map.items = (MAP_ITEM *) malloc(itemsSize);
    if (map.items == NULL)
        return map;

    memcpy(map.items, m->items, itemsSize);
    map.size = m->size;
    map.capacity = m->capacity;
    map.capacityIndex = m->capacityIndex;

    return map;
}


/**
 * Adds item safely; assuming:
 *   - enough capacity,
 *   - no duplicates.
 */
#define ADD_SAFELY MAKE_TOKEN(addSafelyTo, MAP)
int ADD_SAFELY(
    MAP_ITEM *items,
    size_t capacity,
    MAP_ITEM *srcItem
) {
    MAP_ITEM *dstItem;
#ifdef KEY_BY_PTR
    for (size_t i = KEY_HASH(&srcItem->key, capacity); ; i = (i + 1) % capacity) {
#else
    for (size_t i = KEY_HASH(srcItem->key, capacity); ; i = (i + 1) % capacity) {
#endif
        dstItem = items + i;
        if(!dstItem->isPresent)
            break;
    }

    dstItem->isPresent = 1;
    if (dstItem != srcItem) {
        dstItem->key = srcItem->key;
        dstItem->value = srcItem->value;
    }

    return 0;
}


#define CHANGE_CAPACITY_INDEX MAKE_TOKEN(changeCapacityIndexOf, MAP)
static int CHANGE_CAPACITY_INDEX(MAP *m, int capacityIndex) {
    if (capacityIndex < MIN_MAP_CAPACITY_INDEX)
        return 0;

    if (capacityIndex > MAX_MAP_CAPACITY_INDEX)
        return -1;

    size_t capacity = capacities[capacityIndex];
    size_t itemsSize = sizeof(MAP_ITEM) * capacity;
    MAP_ITEM *items = (MAP_ITEM *) malloc(itemsSize);
    if (items == NULL)
        return -1;

    /* Implicitly set all items not present. */
    memset(items, 0, itemsSize);

    /* Re-hash everything */
    for (size_t i = 0; i < m->capacity; ++i) {
        MAP_ITEM *item = m->items + i;
        if (!item->isPresent)
            continue;

        ADD_SAFELY(
           items,
           capacity,
           item
        );
    }

    free(m->items);

    m->items = items;
    m->capacity = capacity;
    m->capacityIndex = capacityIndex;

    return 0;
}


MAP_ITEM *ADD(
    MAP *m,
#ifdef KEY_BY_PTR
    MAP_KEY *key,
#else
    MAP_KEY key,
#endif
#ifdef VALUE_BY_PTR
    MAP_VALUE *value
#else
    MAP_VALUE value
#endif
) {
    size_t newSize = m->size + 1;
    if (newSize >= capacities[m->capacityIndex - 1])
        if (CHANGE_CAPACITY_INDEX(m, m->capacityIndex + 1) != 0)
            return NULL;

    size_t i;
    MAP_ITEM *item;
    for (i = KEY_HASH(key, m->capacity); ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            break;
#ifdef KEY_BY_PTR
        if (KEYS_EQUAL(&item->key, key)) {
#else
        if (KEYS_EQUAL(item->key, key)) {
#endif


#ifdef VALUE_BY_PTR
            item->value = *value;
#else
            item->value = value;
#endif

            return item;
        }
    }

#ifdef KEY_BY_PTR
    item->key = *key;
#else
    item->key = key;
#endif

#ifdef VALUE_BY_PTR
    item->value = *value;
#else
    item->value = value;
#endif

    item->isPresent = 1;

    ++(m->size);

    return item;
}


#ifdef KEY_BY_PTR
MAP_ITEM *GET_ITEM(MAP *m, MAP_KEY *key) {
#else
MAP_ITEM *GET_ITEM(MAP *m, MAP_KEY key) {
#endif
    size_t i;
    MAP_ITEM *item;
    for (i = KEY_HASH(key, m->capacity); ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            return NULL;
#ifdef KEY_BY_PTR
        if (KEYS_EQUAL(&item->key, key))
#else
        if (KEYS_EQUAL(item->key, key))
#endif
            return item;
    }

    return NULL;
}


#ifdef KEY_BY_PTR
MAP_VALUE *GET_VALUE_PTR(MAP *m, MAP_KEY *key) {
#else
MAP_VALUE *GET_VALUE_PTR(MAP *m, MAP_KEY key) {
#endif
    size_t i;
    MAP_ITEM *item;
    for (i = KEY_HASH(key, m->capacity); ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            return NULL;
#ifdef KEY_BY_PTR
        if (KEYS_EQUAL(&item->key, key))
#else
        if (KEYS_EQUAL(item->key, key))
#endif
            return &item->value;
    }

    return NULL;
}


#ifdef KEY_BY_PTR
MAP_VALUE GET_VALUE(MAP *m, MAP_KEY *key) {
#else
MAP_VALUE GET_VALUE(MAP *m, MAP_KEY key) {
#endif
    size_t i;
    MAP_ITEM *item;
    for (i = KEY_HASH(key, m->capacity); ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            return (const MAP_VALUE) {0};
#ifdef KEY_BY_PTR
        if (KEYS_EQUAL(&item->key, key))
#else
        if (KEYS_EQUAL(item->key, key))
#endif
            return item->value;
    }

    return (const MAP_VALUE) {0};
}


#ifdef KEY_BY_PTR
int REMOVE(MAP *m, MAP_KEY *key) {
#else
int REMOVE(MAP *m, MAP_KEY key) {
#endif
    size_t i;
    MAP_ITEM *item;
    for (i = KEY_HASH(key, m->capacity); ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            return 1;
#ifdef KEY_BY_PTR
        if (KEYS_EQUAL(&item->key, key))
#else
        if (KEYS_EQUAL(item->key, key))
#endif
            break;
    }

    item->isPresent = 0;
    --(m->size);

    for (i = (i + 1) % m->capacity; ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            break;

        item->isPresent = 0;
        ADD_SAFELY(
           m->items,
           m->capacity,
           item
        );
    }

    /* Contract if necessary. */
    if (m->size < capacities[m->capacityIndex - 3])
        if (CHANGE_CAPACITY_INDEX(m, m->capacityIndex - 1) != 0)
            return -1;

    return 0;
}


int REDUCE(MAP *m) {
    int capacityIndex = m->capacityIndex;
    while (capacityIndex >= MIN_MAP_CAPACITY_INDEX && m->size < capacities[capacityIndex - 3])
        --capacityIndex;
    if (capacityIndex < MIN_MAP_CAPACITY_INDEX)
        capacityIndex = MIN_MAP_CAPACITY_INDEX;

    if (CHANGE_CAPACITY_INDEX(m, capacityIndex) != 0)
        return -1;

    return 0;
}


int CLEAR(MAP *m) {
    if (m->capacityIndex == MIN_MAP_CAPACITY_INDEX) {
        memset(m->items, 0, sizeof(MAP_ITEM) * m->capacity);
        m->size = 0;

        return 0;
    }

    free(m->items);
    if (INIT(m, 0) != 0)
        return -1;

    return 0;
}


MAP_ITERATOR GET_ITERATOR(MAP *m) {
    return (MAP_ITERATOR) {
        .item = m->items,
        .end =  m->items + m->capacity
    };
}


MAP_ITEM *NEXT_ITEM(MAP_ITERATOR *it) {
    for ( ; it->item != it->end; ++(it->item))
        if (it->item->isPresent)
            return (it->item)++;

    return NULL;
}


void REMOVE_ITEM(MAP *m, MAP_ITERATOR *it) {
    MAP_ITEM *item = it->item - 1;
    assert(item->isPresent);
    item->isPresent = 0;
    --(m->size);
    /* Decrement the iterator, as previous item may become present. */
    it->item = item;

    size_t i = item - m->items;
    for (i = (i + 1) % m->capacity; ; i = (i + 1) % m->capacity) {
        item = m->items + i;
        if (!item->isPresent)
            break;

        item->isPresent = 0;
        ADD_SAFELY(
           m->items,
           m->capacity,
           item
        );
    }
}


#undef CHANGE_CAPACITY_INDEX
#undef ADD_SAFELY
#undef FIND_CAPACITY_INDEX
#undef MAX_MAP_CAPACITY_INDEX
#undef MIN_MAP_CAPACITY_INDEX


#include "map_undef.h"
