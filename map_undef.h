/**
 * All common Map un-definitions.
 */

#undef REMOVE_ITEM
#undef NEXT_ITEM
#undef GET_ITERATOR
#undef REDUCE
#undef CLEAR
#undef REMOVE
#undef GET_VALUE_PTR
#undef GET_VALUE
#undef GET_ITEM
#undef ADD
#undef COPY
#undef MAKE_SIZED
#undef MAKE
#undef RELEASE
#undef INIT

#undef KEYS_EQUAL
#undef EXTERNAL_KEYS_EQUAL
#undef BIG_KEYS_EQUAL
#undef SMALL_KEYS_EQUAL

#undef KEY_HASH
#undef EXTERNAL_KEY_HASH
#undef BIG_KEY_HASH
#undef SMALL_KEY_HASH

#undef MAP_ITERATOR
#undef MAP_ITEM
#undef MAP

#undef KEY_BY_PTR
#undef VALUE_BY_PTR
#undef MAP_KEY
#undef MAP_VALUE

#undef MAP_DEFINE_H
