#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "dset/dset_int.h"


void testInitDset(void **state) {
    Dset_int ds;
    initDset_int(&ds, 123);
    assert_non_null(ds.items.items);
    assert_true(ds.items.capacity >= 123);

    releaseDset_int(&ds);
}


void testReleaseDset(void **state) {
    Dset_int ds;
    initDset_int(&ds, 123);
    releaseDset_int(&ds);
    assert_null(ds.items.items);
    assert_int_equal(ds.items.capacity, 0);
}


void testMakeDset(void **state) {
    Dset_int ds = makeDset_int();
    assert_non_null(ds.items.items);
    assert_true(ds.items.capacity > 0);

    releaseDset_int(&ds);
}


void testMakeSizedDset(void **state) {
    Dset_int ds = makeSizedDset_int(123);
    assert_non_null(ds.items.items);
    assert_true(ds.items.capacity >= 123);

    releaseDset_int(&ds);
}


void testCopyDset(void **state) {
    Dset_int ds = makeSizedDset_int(123);
    for (int i = 0; i < 234; ++i)
        addToDset_int(&ds, i);

    for (int i = 0; i < 234; i += 13)
        unionDset_int(&ds, i, i / 11);

    Dset_int dsCopy = copyDset_int(&ds);

    assert_non_null(dsCopy.items.items);
    assert_ptr_not_equal(ds.items.items, dsCopy.items.items);
    assert_int_equal(ds.items.size, 234);
    assert_int_equal(ds.items.size, dsCopy.items.size);
    assert_int_equal(ds.items.capacity, dsCopy.items.capacity);
    assert_memory_equal(ds.items.items, dsCopy.items.items, ds.items.capacity * sizeof(DsetItem_int));

    releaseDset_int(&dsCopy);
    releaseDset_int(&ds);
}


void testAddToDset(void **state) {
    Dset_int ds = makeSizedDset_int(123);

    DsetItem_int *dsi1 = addToDset_int(&ds, 5);
    assert_int_equal(ds.items.size, 1);
    assert_non_null(dsi1);
    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 5);
    assert_int_equal(dsi1->rank, 0);

    DsetItem_int *dsi2 = addToDset_int(&ds, 5);
    assert_int_equal(ds.items.size, 1);
    assert_ptr_equal(dsi2, dsi1);
    assert_int_equal(dsi2->value, 5);
    assert_int_equal(dsi2->parent, 5);
    assert_int_equal(dsi2->rank, 0);

    DsetItem_int *dsi3 = addToDset_int(&ds, 6);
    assert_int_equal(ds.items.size, 2);
    assert_ptr_not_equal(dsi3, dsi1);
    assert_int_equal(dsi3->value, 6);
    assert_int_equal(dsi3->parent, 6);
    assert_int_equal(dsi3->rank, 0);

    releaseDset_int(&ds);
}


void testGetFromDset(void **state) {
    Dset_int ds = makeDset_int();

    assert_null(getFromDset_int(&ds, 5));

    DsetItem_int *dsi1 = addToDset_int(&ds, 5);
    assert_non_null(dsi1);
    DsetItem_int *dsi1a = getFromDset_int(&ds, 5);
    assert_int_equal(ds.items.size, 1);
    assert_non_null(dsi1a);
    assert_ptr_equal(dsi1a, dsi1);
    assert_int_equal(dsi1a->value, 5);
    assert_int_equal(dsi1a->parent, 5);
    assert_int_equal(dsi1a->rank, 0);

    DsetItem_int *dsi2 = addToDset_int(&ds, 15);
    assert_non_null(dsi2);
    assert_ptr_not_equal(dsi2, dsi1);
    DsetItem_int *dsi2a = getFromDset_int(&ds, 15);
    assert_non_null(dsi2a);
    assert_ptr_equal(dsi2a, dsi2);
    assert_int_equal(dsi2a->value, 15);
    assert_int_equal(dsi2a->parent, 15);
    assert_int_equal(dsi2a->rank, 0);
    assert_int_equal(ds.items.size, 2);

    DsetItem_int *dsi3 = addToDset_int(&ds, 150);
    assert_non_null(dsi3);
    DsetItem_int *dsi3a = getFromDset_int(&ds, 150);
    assert_non_null(dsi3a);
    assert_ptr_equal(dsi3a, dsi3);
    assert_int_equal(dsi3a->value, 150);
    assert_int_equal(dsi3a->parent, 150);
    assert_int_equal(dsi3a->rank, 0);
    assert_int_equal(ds.items.size, 3);

    /* Set up tree structure */
    unionDset_int(&ds, 5, 15);
    unionDset_int(&ds, 15, 150);
    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 5);
    assert_int_equal(dsi2->value, 15);
    assert_int_equal(dsi2->parent, 5);
    assert_int_equal(dsi3->value, 150);
    assert_int_equal(dsi3->parent, 15);

    /* Root returned */
    assert_ptr_equal(getFromDset_int(&ds, 5), dsi1);
    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 5);
    assert_int_equal(dsi2->value, 15);
    assert_int_equal(dsi2->parent, 5);
    assert_int_equal(dsi3->value, 150);
    assert_int_equal(dsi3->parent, 15);

    /* Root returned */
    assert_ptr_equal(getFromDset_int(&ds, 15), dsi1);
    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 5);
    assert_int_equal(dsi2->value, 15);
    assert_int_equal(dsi2->parent, 5);
    assert_int_equal(dsi3->value, 150);
    assert_int_equal(dsi3->parent, 15);

    /* Root returned, path compressed */
    assert_ptr_equal(getFromDset_int(&ds, 150), dsi1);
    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 5);
    assert_int_equal(dsi2->value, 15);
    assert_int_equal(dsi2->parent, 5);
    assert_int_equal(dsi3->value, 150);
    assert_int_equal(dsi3->parent, 5);

    releaseDset_int(&ds);
}


void testUnionDset(void **state) {
    Dset_int ds = makeDset_int();


    DsetItem_int *dsi1 = addToDset_int(&ds, 5);
    assert_ptr_equal(unionDset_int(&ds, 5, 5), dsi1);
    assert_int_equal(ds.items.size, 1);
    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 5);
    assert_int_equal(dsi1->rank, 0);

    DsetItem_int *dsi2 = addToDset_int(&ds, 6);
    assert_ptr_equal(unionDset_int(&ds, 6, 6), dsi2);
    assert_int_equal(ds.items.size, 2);
    assert_int_equal(dsi2->value, 6);
    assert_int_equal(dsi2->parent, 6);
    assert_int_equal(dsi2->rank, 0);


    assert_ptr_equal(unionDset_int(&ds, 6, 5), dsi2);
    assert_int_equal(ds.items.size, 2);

    assert_int_equal(dsi2->value, 6);
    assert_int_equal(dsi2->parent, 6);
    assert_int_equal(dsi2->rank, 1);

    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 6);
    assert_int_equal(dsi1->rank, 0);


    DsetItem_int *dsi3 = addToDset_int(&ds, 7);
    assert_ptr_equal(unionDset_int(&ds, 7, 7), dsi3);
    assert_int_equal(ds.items.size, 3);
    assert_int_equal(dsi3->value, 7);
    assert_int_equal(dsi3->parent, 7);
    assert_int_equal(dsi3->rank, 0);

    assert_ptr_equal(unionDset_int(&ds, 6, 7), dsi2);

    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 6);
    assert_int_equal(dsi1->rank, 0);

    assert_int_equal(dsi2->value, 6);
    assert_int_equal(dsi2->parent, 6);
    assert_int_equal(dsi2->rank, 1);

    assert_int_equal(dsi3->value, 7);
    assert_int_equal(dsi3->parent, 6);
    assert_int_equal(dsi3->rank, 0);


    DsetItem_int *dsi4 = addToDset_int(&ds, 8);
    assert_ptr_equal(unionDset_int(&ds, 8, 8), dsi4);
    assert_int_equal(ds.items.size, 4);
    assert_int_equal(dsi4->value, 8);
    assert_int_equal(dsi4->parent, 8);
    assert_int_equal(dsi4->rank, 0);

    assert_ptr_equal(unionDset_int(&ds, 8, 6), dsi2);

    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 6);
    assert_int_equal(dsi1->rank, 0);

    assert_int_equal(dsi2->value, 6);
    assert_int_equal(dsi2->parent, 6);
    assert_int_equal(dsi2->rank, 1);

    assert_int_equal(dsi3->value, 7);
    assert_int_equal(dsi3->parent, 6);
    assert_int_equal(dsi3->rank, 0);

    assert_int_equal(dsi4->value, 8);
    assert_int_equal(dsi4->parent, 6);
    assert_int_equal(dsi4->rank, 0);


    DsetItem_int *dsi5 = addToDset_int(&ds, 9);
    assert_ptr_equal(unionDset_int(&ds, 9, 9), dsi5);
    assert_int_equal(ds.items.size, 5);
    assert_int_equal(dsi5->value, 9);
    assert_int_equal(dsi5->parent, 9);
    assert_int_equal(dsi5->rank, 0);

    assert_ptr_equal(unionDset_int(&ds, 8, 9), dsi2);

    assert_int_equal(dsi1->value, 5);
    assert_int_equal(dsi1->parent, 6);
    assert_int_equal(dsi1->rank, 0);

    assert_int_equal(dsi2->value, 6);
    assert_int_equal(dsi2->parent, 6);
    assert_int_equal(dsi2->rank, 1);

    assert_int_equal(dsi3->value, 7);
    assert_int_equal(dsi3->parent, 6);
    assert_int_equal(dsi3->rank, 0);

    assert_int_equal(dsi4->value, 8);
    assert_int_equal(dsi4->parent, 6);
    assert_int_equal(dsi4->rank, 0);

    assert_int_equal(dsi5->value, 9);
    assert_int_equal(dsi5->parent, 8);
    assert_int_equal(dsi5->rank, 0);


    releaseDset_int(&ds);
}
