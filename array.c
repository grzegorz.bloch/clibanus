#include "array_def.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>



#define INITIAL_ARRAY_CAPACITY 8


int INIT_ARRAY(
    ARRAY *a,
    size_t size,
    size_t capacity
) {
    assert(capacity > 0);
    assert(capacity >= size);

    size_t itemsSize = sizeof(ARRAY_ITEM) * capacity;
    ARRAY_ITEM *items = (ARRAY_ITEM *) malloc(itemsSize);
    if (items == NULL) {
        a->items = NULL;
        a->size = 0;
        a->capacity = 0;

        return -1;
    }

    a->items = items;
    a->size = size;
    a->capacity = capacity;

    return 0;
}


void RELEASE_ARRAY(ARRAY *a) {
    if (a->items != NULL) {
        free(a->items);
        a->items = NULL;
    }
    a->size = 0;
    a->capacity = 0;
}


ARRAY MAKE_ARRAY(void) {
    ARRAY array;
    (void) INIT_ARRAY(&array, 0, INITIAL_ARRAY_CAPACITY);
    return array;
}


ARRAY MAKE_SIZED_ARRAY(size_t size) {
    ARRAY array;
    (void) INIT_ARRAY(&array, size, size);
    return array;
}


ARRAY COPY_ARRAY(ARRAY *a) {
    ARRAY array = {
        .items = NULL,
        .size = 0,
        .capacity = 0
    };

    if (a->items == NULL)
        return array;

    size_t itemsSize = sizeof(ARRAY_ITEM) * a->capacity;
    array.items = (ARRAY_ITEM *) malloc(itemsSize);
    if (array.items == NULL)
        return array;

    memcpy(array.items, a->items, itemsSize);
    array.size = a->size;
    array.capacity = a->capacity;

    return array;
}


#define EXTEND_ARRAY MAKE_TOKEN(extend, ARRAY)
static int EXTEND_ARRAY(ARRAY *a, size_t size) {
    assert(size > a->capacity);

    size_t newCapacity = a->capacity << 1;
    while (newCapacity < size)
        newCapacity <<= 1;

    size_t itemsSize = sizeof(ARRAY_ITEM) * newCapacity;
    ARRAY_ITEM *items = (ARRAY_ITEM *) malloc(itemsSize);
    if (items == NULL)
        return -1;

    size_t copySize = sizeof(ARRAY_ITEM) * a->capacity;
    memcpy(items, a->items, copySize);

    free(a->items);

    a->items = items;
    a->capacity = newCapacity;

    return 0;
}


#define ENSURE_ARRAY_CAPACITY(a) { \
    size_t newSize = (a)->size + 1; \
    if (newSize > (a)->capacity) \
        if (EXTEND_ARRAY((a), newSize) != 0) \
            return NULL; \
}


ARRAY_ITEM *ADD_TO_ARRAY(
    ARRAY *a,
    ARRAY_ITEM item
) {
    ENSURE_ARRAY_CAPACITY(a)

    ARRAY_ITEM *pi = a->items + a->size++;
    *pi = item;

    return pi;
}


ARRAY_ITEM *ADD_BY_PTR_TO_ARRAY(
    ARRAY *a,
    ARRAY_ITEM *item
) {
    ENSURE_ARRAY_CAPACITY(a)

    ARRAY_ITEM *pi = a->items + a->size++;
    *pi = *item;

    return pi;
}


ARRAY_ITEM *ADD_EMPTY_TO_ARRAY(ARRAY *a) {
    ENSURE_ARRAY_CAPACITY(a)

    return a->items + a->size++;
}


int REDUCE_ARRAY(ARRAY *a) {
    if (a->capacity <= INITIAL_ARRAY_CAPACITY)
        return 0;

    if (a->size > a->capacity >> 2)
        return 0;

    size_t minCapacity = a->size << 1;
    size_t capacity = INITIAL_ARRAY_CAPACITY;
    while (capacity < minCapacity)
        capacity <<= 1;

    if (capacity >= a->capacity)
        return 0;

    size_t itemsSize = sizeof(ARRAY_ITEM) * capacity;
    ARRAY_ITEM *items = (ARRAY_ITEM *) malloc(itemsSize);
    if (items == NULL)
        return -1;

    size_t copySize = sizeof(ARRAY_ITEM) * a->size;
    memcpy(items, a->items, copySize);

    free(a->items);

    a->items = items;
    a->capacity = capacity;

    return 0;
}


int RESIZE_ARRAY(
    ARRAY *a,
    size_t size
) {
    assert(size > 0);

    if (a->capacity == size) {
        a->size = size;
        return 0;
    }

    size_t itemsSize = sizeof(ARRAY_ITEM) * size;
    ARRAY_ITEM *items = (ARRAY_ITEM *) malloc(itemsSize);
    if (items == NULL)
        return -1;

    size_t copySize = size <= a->size ? itemsSize : sizeof(ARRAY_ITEM) * a->size;
    memcpy(items, a->items, copySize);

    free(a->items);

    a->items = items;
    a->size = size;
    a->capacity = size;

    return 0;
}


#undef ENSURE_ARRAY_CAPACITY
#undef EXTEND_ARRAY
#undef INITIAL_ARRAY_CAPACITY


#include "array_undef.h"
