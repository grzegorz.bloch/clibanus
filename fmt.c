#include "fmt.h"

#include <stdlib.h>


#define FMT_SEPARATOR '.'
#define FMT_BUFFER_SIZE 512


#define I_TO_HEX_C(i) \
    (i) < 10 ? (i) + 48 : (i) + 87

#define HIGHER_HEX_C(b) \
    I_TO_HEX_C(((b) >> 4))

#define LOWER_HEX_C(b) \
    I_TO_HEX_C(((b) & 0x0f))


int sprinth_byte(
    char *buffer,
    uint8_t byte
) {
    /* Most significant bits on the left side. */
    *buffer = HIGHER_HEX_C(byte);
    *++buffer = LOWER_HEX_C(byte);

    return 2;
}


int sprinth(
    char *buffer,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    if (numSeparated <= 0) {
        numSeparated = numBytes;
    }

    int n = 0;
    /* Most significant byte on the left side. */
    bytes += numBytes;
    for (int i = 0; i < numBytes; ++i) {
        if (i % numSeparated == 0) {
            *buffer++ = FMT_SEPARATOR;
            n++;
        }
        n += sprinth_byte(buffer, *((char *) --bytes));
        buffer += 2;
    }
    *buffer = '\0';

    return n;
}


int fprinth(
    FILE *fp,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    if (numSeparated <= 0) {
        numSeparated = numBytes;
    }

    char stackBuffer[FMT_BUFFER_SIZE];
    char *heapBuffer = NULL;
    char *buffer = NULL;
    int size = 2 * numBytes + numBytes / numSeparated + 1;
    if (size <= FMT_BUFFER_SIZE) {
        buffer = stackBuffer;
    }
    else {
        heapBuffer = malloc(size);
        if (heapBuffer == NULL)
            return -1;
        buffer = heapBuffer;
    }

    sprinth(
        buffer,
        bytes,
        numBytes,
        numSeparated
    );

    int n = fprintf(fp, "%s", buffer);

    if (heapBuffer != NULL)
        free(buffer);

    return n;
}


int printh(
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return fprinth(stdout, bytes, numBytes, numSeparated);
}


int fprinthln(
    FILE *fp,
    void *bytes,
    int numBytes,
    int numSeparated
) {
    int n = fprinth(fp, bytes, numBytes, numSeparated);
    if (n < 0)
        return -1;

    if (fprintf(fp, "\n") < 0)
        return -1;

    return n + 1;
}


int printhln(
    void *bytes,
    int numBytes,
    int numSeparated
) {
    return fprinthln(stdout, bytes, numBytes, numSeparated);
}


int printh_uint32_t(uint32_t word) {
    return printh(&word, sizeof(word), 1);
}


int printh_uint64_t(uint64_t word) {
    return printh(&word, sizeof(word), 1);
}
