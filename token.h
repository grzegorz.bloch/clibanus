#ifndef TOKEN_H
#define TOKEN_H


#define CONCATENATE(left, right) left ## right

#define SEPARATE(left, right) left ## _ ## right

#define MAKE_TOKEN(left, right) CONCATENATE(left, right)

#define MAKE_SEPARATED_TOKEN(left, right) SEPARATE(left, right)


#define CONCATENATE_3(left, middle, right) left ## middle ## right

#define SEPARATE_3(left, middle, right) left ## _ ## middle ## _ ## right

#define MAKE_TOKEN_3(left, middle, right) SEPARATE_3(left, middle, right)

#define MAKE_SEPARATED_TOKEN_3(left, middle, right) SEPARATE_3(left, middle, right)


#endif /* TOKEN_H */
