/**
 * Generically typed, openly addressed, dynamic hash map.
 */

#include <stddef.h>
#include <stdint.h>

#include "map_def.h"


struct MAP_ITEM {
    MAP_KEY key;
    MAP_VALUE value;
    uint8_t isPresent;
};
typedef struct MAP_ITEM MAP_ITEM;


struct MAP {
    MAP_ITEM *items;
    size_t size;
    size_t capacity;
    int capacityIndex;
};
typedef struct MAP MAP;


struct MAP_ITERATOR {
    MAP_ITEM *item;
    MAP_ITEM *end;
};
typedef struct MAP_ITERATOR MAP_ITERATOR;


int INIT(MAP *m, size_t targetSize);

void RELEASE(MAP *m);

MAP MAKE(void);

MAP MAKE_SIZED(size_t targetSize);

MAP COPY(MAP *m);

MAP_ITEM *ADD(
    MAP *m,
#ifdef KEY_BY_PTR
    MAP_KEY *key,
#else
    MAP_KEY key,
#endif
#ifdef VALUE_BY_PTR
    MAP_VALUE *value
#else
    MAP_VALUE value
#endif
);

#ifdef KEY_BY_PTR
MAP_VALUE *GET_VALUE_PTR(MAP *m, MAP_KEY *key);
#else
MAP_VALUE *GET_VALUE_PTR(MAP *m, MAP_KEY key);
#endif

#ifdef KEY_BY_PTR
MAP_VALUE GET_VALUE(MAP *m, MAP_KEY *key);
#else
MAP_VALUE GET_VALUE(MAP *m, MAP_KEY key);
#endif

#ifdef KEY_BY_PTR
MAP_ITEM *GET_ITEM(MAP *m, MAP_KEY *key);
#else
MAP_ITEM *GET_ITEM(MAP *m, MAP_KEY key);
#endif

#ifdef KEY_BY_PTR
int REMOVE(MAP *m, MAP_KEY *key);
#else
int REMOVE(MAP *m, MAP_KEY key);
#endif

int CLEAR(MAP *m);

int REDUCE(MAP *m);


MAP_ITERATOR GET_ITERATOR(MAP *m);

MAP_ITEM *NEXT_ITEM(MAP_ITERATOR *it);

/**
 * Removes most recent item returned by NEXT_ITEM.
 * Iterator remains valid.
 * Once iterating has finished REDUCE should be called.
 */
void REMOVE_ITEM(MAP *m, MAP_ITERATOR *it);


#include "map_undef.h"
