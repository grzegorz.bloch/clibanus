#include "channel_def.h"

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


int INIT_CHANNEL(CHANNEL *ch, size_t capacity) {
    assert(capacity > 0);

    size_t itemsSize = sizeof(CHANNEL_ITEM) * capacity;
    CHANNEL_ITEM *items = (CHANNEL_ITEM *) malloc(itemsSize);
    if (items == NULL) {
        memset(ch, 0, sizeof(CHANNEL));

        return -1;
    }

    ch->items = items;
    ch->capacity = capacity;
    ch->size = 0;
    ch->head = 0;
    ch->tail = capacity - 1;

    if (pthread_mutex_init(&ch->lock, NULL) != 0)
        return -1;
    if (pthread_cond_init(&ch->notEmpty, NULL) != 0)
        return -1;
    if (pthread_cond_init(&ch->notFull, NULL) != 0)
        return -1;

    return 0;
}


int RELEASE_CHANNEL(CHANNEL *ch) {
    if (pthread_cond_destroy(&ch->notFull) != 0)
        return -1;
    if (pthread_cond_destroy(&ch->notEmpty) != 0)
        return -1;
    if (pthread_mutex_destroy(&ch->lock) != 0)
        return -1;

    if (ch->items != NULL) {
        free(ch->items);
    }
    memset(ch, 0, sizeof(CHANNEL));

    return 0;
}


CHANNEL MAKE_CHANNEL(size_t capacity) {
    CHANNEL channel;
    (void) INIT_CHANNEL(&channel, capacity);
    return channel;
}


#define GET_TIMEOUT MAKE_TOKEN(getTimeoutFor, CHANNEL)
static void GET_TIMEOUT(struct timespec *ts, long ms) {
    timespec_get(ts, TIME_UTC);
    ts->tv_sec += ms / 1000;
    long long ns = ts->tv_nsec;
    ns += (ms % 1000) * 1000000;
    ts->tv_sec += ns / 1000000000;
    ts->tv_nsec = ns % 1000000000;
}


#define ADD_SAFELY_TO_CHANNEL() { \
    ch->tail++; \
    ch->tail %= ch->capacity; \
    ch->size++; \
\
    CHANNEL_ITEM *pi = ch->items + ch->tail; \
    *pi = item; \
\
    pthread_mutex_unlock(&ch->lock); \
    pthread_cond_signal(&ch->notEmpty); \
}


void ADD_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM item) {
    pthread_mutex_lock(&ch->lock);
    while (ch->size >= ch->capacity)
        pthread_cond_wait(&ch->notFull, &ch->lock);

    ADD_SAFELY_TO_CHANNEL();
}


int ADD_TIMED_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM item, long ms) {
    pthread_mutex_lock(&ch->lock);
    struct timespec ts;
    GET_TIMEOUT(&ts, ms);
    while (ch->size >= ch->capacity) {
        if (pthread_cond_timedwait(&ch->notFull, &ch->lock, &ts) != 0) {
            pthread_mutex_unlock(&ch->lock);
            return -1;
        }
    }

    ADD_SAFELY_TO_CHANNEL();

    return 0;
}


#define ADD_SAFELY_BY_PTR_TO_CHANNEL() { \
    ch->tail++; \
    ch->tail %= ch->capacity; \
    ch->size++; \
\
    CHANNEL_ITEM *pi = ch->items + ch->tail; \
    *pi = *item; \
\
    pthread_mutex_unlock(&ch->lock); \
    pthread_cond_signal(&ch->notEmpty); \
}


void ADD_BY_PTR_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM *item) {
    pthread_mutex_lock(&ch->lock);
    while (ch->size >= ch->capacity)
        pthread_cond_wait(&ch->notFull, &ch->lock);

    ADD_SAFELY_BY_PTR_TO_CHANNEL();
}


int ADD_TIMED_BY_PTR_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM *item, long ms) {
    pthread_mutex_lock(&ch->lock);
    struct timespec ts;
    GET_TIMEOUT(&ts, ms);
    while (ch->size >= ch->capacity) {
        if (pthread_cond_timedwait(&ch->notFull, &ch->lock, &ts) != 0) {
            pthread_mutex_unlock(&ch->lock);
            return -1;
        }
    }

    ADD_SAFELY_BY_PTR_TO_CHANNEL();

    return 0;
}


#define GET_SAFELY_FROM_CHANNEL() { \
    CHANNEL_ITEM item = *(ch->items + ch->head); \
\
    ch->head++; \
    ch->head %= ch->capacity; \
    ch->size--; \
\
    pthread_mutex_unlock(&ch->lock); \
    pthread_cond_signal(&ch->notFull); \
\
    return item; \
}


CHANNEL_ITEM GET_FROM_CHANNEL(CHANNEL *ch) {
    pthread_mutex_lock(&ch->lock);
    while (ch->size <= 0)
        pthread_cond_wait(&ch->notEmpty, &ch->lock);

    GET_SAFELY_FROM_CHANNEL();
}


CHANNEL_ITEM GET_TIMED_FROM_CHANNEL(CHANNEL *ch, long ms) {
    pthread_mutex_lock(&ch->lock);
    struct timespec ts;
    GET_TIMEOUT(&ts, ms);
    while (ch->size <= 0) {
        if (pthread_cond_timedwait(&ch->notEmpty, &ch->lock, &ts) != 0) {
            pthread_mutex_unlock(&ch->lock);
            return (const CHANNEL_ITEM) {0};
        }
    }

    GET_SAFELY_FROM_CHANNEL();
}


#undef GET_TIMEOUT
#undef ADD_SAFELY_TO_CHANNEL
#undef GET_SAFELY_FROM_CHANNEL

#include "channel_undef.h"
