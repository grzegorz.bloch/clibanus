/**
 * All common Queue un-definitions.
 */


#undef GET_PTR_FROM_QUEUE
#undef GET_FROM_QUEUE
#undef REMOVE_FROM_QUEUE
#undef ADD_BY_PTR_TO_QUEUE
#undef ADD_TO_QUEUE
#undef COPY_QUEUE
#undef MAKE_SIZED_QUEUE
#undef MAKE_QUEUE
#undef RELEASE_QUEUE
#undef INIT_QUEUE

#undef QUEUE
#undef QUEUE_ITEM


#undef QUEUE_DEFINE_H
