# clibanus

C library with tools essential for day-to-day programming:
- generic collections in C:
    - Array
    - Map
    - Queue
    - Channel
- logging
- formatting of binary data

## Array
- Is generically typed.
- Has continuous internal storage.
- Has dynamic capacity with constant, `O(1)`, amortised cost of adding and removing elements.
- Supports custom naming conventions.

### Array usage
```c
/* In your header file */
struct IntPair {
    int first;
    int second;
};
typedef struct IntPair IntPair;

#define ARRAY_ITEM IntPair
#include "array.h"

void printArray_IntPair(Array_IntPair *array);

/* In your C file */
#define ARRAY_ITEM IntPair
#include "array.c"

/* In your main file */
int main() {
    /* Create an empty array with default capacity. */
    Array_IntPair array = makeArray_IntPair();
    printArray_IntPair(&array);
    /* []: size: 0, capacity: 8 */

    IntPair pair = { .first = 1, .second = 10};
    addToArray_IntPair(&array, pair);
    printArray_IntPair(&array);
    /* [(1, 10)]: size: 1, capacity: 8 */

    IntPair anotherPair = { .first = 2, .second = 20};
    addByPtrToArray_IntPair(&array, &anotherPair);
    printArray_IntPair(&array);
    /* [(1, 10), (2, 20)]: size: 2, capacity: 8 */

    IntPair *emptyPair = addEmptyToArray_IntPair(&array);
    emptyPair->first = 3;
    emptyPair->second = 30;
    printArray_IntPair(&array);
    /* [(1, 10), (2, 20), (3, 30)]: size: 3, capacity: 8 */

    for (int i = 4; i <= 9; i++)
        addToArray_IntPair(&array, (IntPair) {i, 10 * i});
    printArray_IntPair(&array);
    /* [(1, 10), (2, 20), (3, 30), (4, 40), (5, 50), (6, 60), (7, 70), (8, 80), (9, 90)]: size: 9, capacity: 16 */

    /* Underlying array.items is just an ordinary C array */
    for (size_t i = 0; i < array.size; i++)
        printf("%i: (%d, %d)\n", i, array.items[i].first, array.items[i].second);

    for (int i = 10; i <= 1000; i++)
        addToArray_IntPair(&array, (IntPair) {i, 10 * i});
    printArray_IntPair(&array);
    /* [(1, 10), ..., (1000, 10000)]: size: 1000, capacity: 1024 */

    /* Set size without changing capacity */
    array.size = 9;
    printArray_IntPair(&array);
    /* [(1, 10), (2, 20), (3, 30), (4, 40), (5, 50), (6, 60), (7, 70), (8, 80), (9, 90)]: size: 9, capacity: 1024 */

    /* Adjust capacity to the current size. */
    reduceArray_IntPair(&array);
    printArray_IntPair(&array);
    /* [(1, 10), (2, 20), (3, 30), (4, 40), (5, 50), (6, 60), (7, 70), (8, 80), (9, 90)]: size: 9, capacity: 32 */

    /* Set the same size and capacity */
    resizeArray_IntPair(&array, 5);
    printArray_IntPair(&array);
    /* [(1, 10), (2, 20), (3, 30), (4, 40), (5, 50)]: size: 5, capacity: 5 */

    releaseArray_IntPair(&array);

    return 0;
}
```

with `printArray_IntPair` defined in your C file, as follows:
```c
#include <stdio.h>

void printArray_IntPair(Array_IntPair *array) {
    if (array->size == 0) {
        printf("[]: size: %ld, capacity: %ld\n", array->size, array->capacity);
        return;
    }

    if (array->size > 20) {
        IntPair *lastIntPair = array->items + array->size - 1;
        printf(
            "[(%d, %d), ..., (%d, %d)]: size: %ld, capacity: %ld\n",
            array->items->first, array->items->second,
            lastIntPair->first, lastIntPair->second,
            array->size,
            array->capacity
        );
        return;
    }

    printf("[");
    for (size_t i = 0; i < array->size - 1; i++)
        printf("(%d, %d), ", array->items[i].first, array->items[i].second);
    IntPair *lastIntPair = array->items + array->size - 1;
    printf("(%d, %d)", lastIntPair->first, lastIntPair->second);
    printf("]: size: %ld, capacity: %ld\n", array->size, array->capacity);
}
```

### Creating Arrays
```c
    /* Create an empty array with default capacity. */
    Array_IntPair array = makeArray_IntPair();

    /* Create an array with the same size and capacity. */
    Array_IntPair sizedArray = makeSizedArray_IntPair(100);

    /* Create an array of arbitrary size and arbitrary capacity. */
    Array_IntPair customArray;
    initArray_IntPair(&customArray, 123, 12345);
```

### Overriding Array's default naming convention
In order to override the default naming convention -- `Array_IntPair` -- you need to define the name of your array explicitly, for example: `#define ARRAY IntPairArray`:

```c
struct IntPair {
    int first;
    int second;
};
typedef struct IntPair IntPair;

#define ARRAY_ITEM IntPair
#define ARRAY IntPairArray
#include "array.h"

void printIntPairArray(IntPairArray *array);


#define ARRAY_ITEM IntPair
#define ARRAY IntPairArray
#include "array.c"


int main() {
    IntPairArray array = makeIntPairArray();
    printIntPairArray(&array);
    /* []: size: 0, capacity: 8 */

    addToIntPairArray(&array, (IntPair) { .first = 1, .second = 10});
    printIntPairArray(&array);
    /* [(1, 10)]: size: 1, capacity: 8 */

    releaseIntPairArray(&array);

    return 0;
}

```

### Array error handling
```c
int main() {
    Array_IntPair array = makeArray_IntPair();
    if (array.items == NULL)
        return 1;

    Array_IntPair sizedArray = makeSizedArray_IntPair(100);
    if (sizedArray.items == NULL)
        return 1;
    releaseArray_IntPair(&sizedArray);

    Array_IntPair customArray;
    if (initArray_IntPair(&customArray, 123, 12345) != 0)
        return 1;
    releaseArray_IntPair(&customArray);

    IntPair pair = { .first = 1, .second = 10};
    if (addToArray_IntPair(&array, pair) == NULL)
        return 1;

    IntPair anotherPair = { .first = 2, .second = 20};
    if (addByPtrToArray_IntPair(&array, &anotherPair) == NULL)
        return 1;

    IntPair *emptyPair = addEmptyToArray_IntPair(&array);
    if (emptyPair == NULL)
        return 1;
    emptyPair->first = 3;
    emptyPair->second = 30;

    if (resizeArray_IntPair(&array, 100) != 0)
        return 1;

    array.size = 3;
    if (reduceArray_IntPair(&array) != 0)
        return 1;

    releaseArray_IntPair(&array);

    return 0;
}
```

### Predefined Arrays

**Clibanus** contains a set of predefined `Array`s of primitive types, located in `array` subdirectory:
- `Array_char`
- `Array_int`
- `Array_float`
- `Array_double`
- `Array_long`
- `Array_int8`
- `Array_int16`
- `Array_int32`
- `Array_int64`
- `Array_uint8`
- `Array_uint16`
- `Array_uint32`
- `Array_uint64`

## Map
- Is generically typed.
- Has dynamic capacity.
- Implements open addressing algorithm.
- Provides default hash functions for:
    - small keys that may be cast to `size_t`,
    - big keys that cannot be cast to `size_t`.
- Allows for custom hash functions.
- Supports iterators.
- Supports custom naming conventions.


### Map usage
```c
/* In your header file */
#define MAP_KEY int
#define MAP_VALUE double
#include "map.h"

void printMap_int_double(Map_int_double *map);

/* In your C file */
#define MAP_KEY int
#define MAP_VALUE double

#include "map.c"

/* In your main file */
#include <stdio.h>

int main() {
    /* Create an empty map with default capacity. */
    Map_int_double map = makeMap_int_double();
    printMap_int_double(&map);
    /* []: size: 0, capacity: 13 */

    addToMap_int_double(&map, 123, 1.23);
    printMap_int_double(&map);
    /* [123: 1.23]: size: 1, capacity: 13 */

    addToMap_int_double(&map, 234, 2.34);
    printMap_int_double(&map);
    /* [234: 2.34, 123: 1.23]: size: 2, capacity: 13 */

    addToMap_int_double(&map, 345, 3.45);
    printMap_int_double(&map);
    /* [234: 2.34, 123: 1.23, 345: 3.45]: size: 3, capacity: 13 */

    addToMap_int_double(&map, 456, 4.56);
    printMap_int_double(&map);
    /* [234: 2.34, 456: 4.56, 123: 1.23, 345: 3.45]: size: 4, capacity: 13 */

    addToMap_int_double(&map, 567, 5.67);
    printMap_int_double(&map);
    /* [234: 2.34, 456: 4.56, 123: 1.23, 345: 3.45, 567: 5.67]: size: 5, capacity: 13 */

    double v123 = getValueFromMap_int_double(&map, 123);
    printf("Value for %d: %.2f\n", 123, v123);
    /* Value for 123: 1.23 */

    double *pv123 = getValuePtrFromMap_int_double(&map, 123);
    printf("Value ptr for %d: 0x%016lx -> %.2f\n", 123, pv123, *pv123);
    /* Value ptr for 123: 0x00005574b0fd5338 -> 1.23 */

    double v1 = getValueFromMap_int_double(&map, 1);
    printf("Value for %d: %.2f\n", 1, v1);
    /* Value for 1: 0.00 */

    double *pv1 = getValuePtrFromMap_int_double(&map, 1);
    printf("Value ptr for %d: 0x%016lx -> _\n", 1, pv1);
    /* Value ptr for 1: 0x0000000000000000 -> _ */


    MapItem_int_double *item123 = getItemFromMap_int_double(&map, 123);
    printf("Item for %d: 0x%016lx -> %d: %.2f\n", 123, item123, item123->key, item123->value);
    /* Item for 123: 0x00005574b0fd5330 -> 123: 1.23 */

    MapItem_int_double *item1 = getItemFromMap_int_double(&map, 1);
    printf("Item for %d: 0x%016lx -> _\n", 1, item1);
    /* Item for 1: 0x0000000000000000 -> _ */

    /* Removing by key */
    removeFromMap_int_double(&map, 345);
    printMap_int_double(&map);
    /* [234: 2.34, 456: 4.56, 123: 1.23, 567: 5.67]: size: 4, capacity: 13 */

    /* Iterating all items */
    IteratorMap_int_double it = getIteratorMap_int_double(&map);
    for (
        MapItem_int_double *item = nextMapItem_int_double(&it);
        item != NULL;
        item = nextMapItem_int_double(&it)
    )
        printf("%d: %.2f\n", item->key, item->value);
    /*
     * 234: 2.34
     * 456: 4.56
     * 123: 1.23
     * 567: 5.67
     */

    /* Removing using iterator */
    it = getIteratorMap_int_double(&map);
    MapItem_int_double *item = nextMapItem_int_double(&it);
    printf("Next item: %d: %.2f\n", item->key, item->value);
    /* Next item: 234: 2.34 */

    item = nextMapItem_int_double(&it);
    printf("Current item to be removed: %d: %.2f\n", item->key, item->value);
    /* Current item to be removed: 456: 4.56 */

    removeMapItem_int_double(&map, &it);
    printMap_int_double(&map);
    /* [234: 2.34, 123: 1.23, 567: 5.67]: size: 3, capacity: 13 */

    item = nextMapItem_int_double(&it);
    printf("Next item: %d: %.2f\n", item->key, item->value);
    /* Next item: 123: 1.23 */

    for (int i = -1000; i < 1000; i += 10)
        addToMap_int_double(&map, i, i * 0.01);
    printMap_int_double(&map);
    /* [0: 0.00, -810: -8.10, -300: -3.00, 510: 5.10, 10: 0.10, -800: -8.00, -290: -2.90, 520: 5.20, 20: 0.20, -790: -7.90, ...]: size: 203, capacity: 509 */

    /* Removing many elements reduces map's capacity */
    for (int i = -1000; i < 750; i += 10)
        removeFromMap_int_double(&map, i);
    printMap_int_double(&map);
    /* [567: 5.67, 910: 9.10, 234: 2.34, 800: 8.00, 123: 1.23, 920: 9.20, 810: 8.10, 930: 9.30, 820: 8.20, 940: 9.40, ...]: size: 28, capacity: 113 */

    releaseMap_int_double(&map);
    printMap_int_double(&map);
    /* []: size: 0, capacity: 0 */
}

```

with ` printMap_int_double` defined in your C file, as follows:
```c
#include <stdio.h>
void printMap_int_double(Map_int_double *map) {
    if (map->size == 0) {
        printf("[]: size: %ld, capacity: %ld\n", map->size, map->capacity);
        return;
    }

    if (map->size >= 10) {
        printf("[");
        IteratorMap_int_double it = getIteratorMap_int_double(map);
        for (int i = 0; i < 10; i++) {
            MapItem_int_double *item = nextMapItem_int_double(&it);
            printf("%d: %.2f, ", item->key, item->value);
        }
        printf("...]: size: %ld, capacity: %ld\n", map->size, map->capacity);
        return;
    }

    printf("[");
    int i = 0;
    IteratorMap_int_double it = getIteratorMap_int_double(map);
    for (
        MapItem_int_double *item = nextMapItem_int_double(&it);
        item != NULL;
        item = nextMapItem_int_double(&it)
    ) {
        printf("%d: %.2f", item->key, item->value);
        if (++i < map->size)
            printf(", ");
    }
    printf("]: size: %ld, capacity: %ld\n", map->size, map->capacity);
}
```


### Map with non-primitive keys

In order to create a `Map` with non-primitive keys, you need to define two functions:
- `EXTERNAL_KEY_HASH`
- `EXTERNAL_KEYS_EQUAL`

The simplest way is to use predefined:
- `BIG_KEY_HASH`
- `BIG_KEYS_EQUAL`

implemented in terms of bitwise operations on individual bytes:

```c
/* In your header file */
struct IntPair {
    int first;
    int second;
};
typedef struct IntPair IntPair;

#define MAP_KEY IntPair
#define MAP_VALUE double
#define EXTERNAL_KEY_HASH BIG_KEY_HASH
#define EXTERNAL_KEYS_EQUAL BIG_KEYS_EQUAL
#include "map.h"

/* In your C file */
#define MAP_KEY IntPair
#define MAP_VALUE double
#define EXTERNAL_KEY_HASH BIG_KEY_HASH
#define EXTERNAL_KEYS_EQUAL BIG_KEYS_EQUAL
#include "map.c"

/* In your main file */
#include <stdio.h>

int main() {
    Map_IntPair_double map = makeMap_IntPair_double();

    addToMap_IntPair_double(&map, (IntPair) {1, 23}, 1.23);
    addToMap_IntPair_double(&map, (IntPair) {2, 34}, 2.34);
    addToMap_IntPair_double(&map, (IntPair) {3, 45}, 3.45);

    double v = getValueFromMap_IntPair_double(&map, (IntPair) {1, 23});

    removeFromMap_IntPair_double(&map, (IntPair) {2, 34});

    releaseMap_IntPair_double(&map);
}
```

### Map with custom keys

In order to create a `Map` with custom keys, you need to define two custom functions:
- `EXTERNAL_KEY_HASH`
- `EXTERNAL_KEYS_EQUAL`

implemented in an arbitrary way, but obeying the general contract:

```c
/* In your header file */
#include <stddef.h>

struct IntPair {
    int first;
    int second;
};
typedef struct IntPair IntPair;

size_t hashIntPair(IntPair *intPair, size_t capacity);
int intPairsEqual(IntPair *lip, IntPair *rip);

#define MAP_KEY IntPair
#define MAP_VALUE double
#define KEY_BY_PTR
#define EXTERNAL_KEY_HASH hashIntPair
#define EXTERNAL_KEYS_EQUAL intPairsEqual
#include "map.h"

/* In your C file */
/* Use only the second field for hashing and equality. */
size_t  hashIntPair(IntPair *intPair, size_t capacity) {
    return (size_t) intPair->second % capacity;
}

int intPairsEqual(IntPair *lip, IntPair *rip) {
    return lip->second == rip->second;
}

#define MAP_KEY IntPair
#define MAP_VALUE double
#define KEY_BY_PTR
#define EXTERNAL_KEY_HASH hashIntPair
#define EXTERNAL_KEYS_EQUAL intPairsEqual
#include "map.c"

/* In your main file */
#include <stdio.h>

int main() {
    Map_IntPair_double map = makeMap_IntPair_double();

    IntPair ip1 = {1, 23};
    addToMap_IntPair_double(&map, &ip1, 1.23);
    printMap_IntPair_double(&map);
    /* [(1, 23): 1.23]: size: 1, capacity: 13 */

    IntPair ip2 = {2, 34};
    addToMap_IntPair_double(&map, &ip2, 2.34);
    printMap_IntPair_double(&map);
    /* [(2, 34): 2.34, (1, 23): 1.23]: size: 2, capacity: 13 */

    /* The same key */
    IntPair ip3 = {3, 23};
    addToMap_IntPair_double(&map, &ip3, 3.45);
    printMap_IntPair_double(&map);
    /* [(2, 34): 2.34, (1, 23): 3.45]: size: 2, capacity: 13 */

    /* Different key */
    IntPair ip4 = {1, 45};
    addToMap_IntPair_double(&map, &ip4, 4.56);
    printMap_IntPair_double(&map);
    /* [(1, 45): 4.56, (2, 34): 2.34, (1, 23): 3.45]: size: 3, capacity: 13 */

    IntPair ip5 = {123456789, 34};
    double v = getValueFromMap_IntPair_double(&map, &ip5);
    printf("Value for (%d, %d): %.2f\n", ip5.first, ip5.second, v);
    /* Value for (123456789, 34): 2.34 */

    releaseMap_IntPair_double(&map);
}
```

# Author

[Grzegorz Bloch](mailto:grzegorz.bloch@gmail.com)

# License

[GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007](https://www.gnu.org/licenses/lgpl-3.0.en.html)
