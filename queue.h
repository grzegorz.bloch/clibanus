/**
 * Generically typed, continuous, dynamic FIFO queue.
 */
#include <stddef.h>

#include "queue_def.h"


struct QUEUE {
    QUEUE_ITEM *items;
    size_t capacity;
    size_t minCapacity;
    size_t size;
    size_t head;
    size_t tail;
};
typedef struct QUEUE QUEUE;


int INIT_QUEUE (
    QUEUE *q,
    size_t capacity
);

void RELEASE_QUEUE(QUEUE *q);

QUEUE MAKE_QUEUE(void);

QUEUE MAKE_SIZED_QUEUE(size_t targetSize);

QUEUE COPY_QUEUE(QUEUE *q);

QUEUE_ITEM *ADD_TO_QUEUE(QUEUE *q, QUEUE_ITEM item);

QUEUE_ITEM *ADD_BY_PTR_TO_QUEUE(QUEUE *q, QUEUE_ITEM *item);

int REMOVE_FROM_QUEUE(QUEUE *q, QUEUE_ITEM *item);

QUEUE_ITEM GET_FROM_QUEUE(QUEUE *q);

QUEUE_ITEM *GET_PTR_FROM_QUEUE(QUEUE *q);


#include "queue_undef.h"
