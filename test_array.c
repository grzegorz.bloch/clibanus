#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "array/array_int.h"


void testInitArray(void **state) {
    Array_int a;
    initArray_int(&a, 54, 543);
    assert_int_equal(a.size, 54);
    assert_int_equal(a.capacity, 543);
    assert_non_null(a.items);

    releaseArray_int(&a);
}


void testReleaseArray(void **state) {
    Array_int a;
    initArray_int(&a, 6, 7);

    releaseArray_int(&a);
    assert_int_equal(a.size, 0);
    assert_int_equal(a.capacity, 0);
    assert_null(a.items);
}


void testMakeArray(void **state) {
    Array_int a = makeArray_int();
    assert_int_equal(a.size, 0);
    assert_int_equal(a.capacity, 8);
    assert_non_null(a.items);

    releaseArray_int(&a);
}


void testMakeSizedArray(void **state) {
    Array_int a = makeSizedArray_int(234);
    assert_int_equal(a.size, 234);
    assert_int_equal(a.capacity, 234);
    assert_non_null(a.items);

    releaseArray_int(&a);
}


void testCopyArray(void **state) {
    Array_int a;
    initArray_int(&a, 54, 543);

    int *da = (int *) a.items;
    for (int i = 0; i < 54; ++i)
        da[i] = i;

    Array_int b = copyArray_int(&a);

    assert_int_equal(b.size, 54);
    assert_int_equal(b.capacity, 543);
    assert_non_null(b.items);
    assert_ptr_not_equal(a.items, b.items);
    assert_memory_equal(a.items, b.items, sizeof(int) * 543);

    releaseArray_int(&b);
    releaseArray_int(&a);
}


void testCopyReleasedArray(void **state) {
    Array_int a;
    initArray_int(&a, 54, 543);
    releaseArray_int(&a);

    Array_int b = copyArray_int(&a);
    assert_int_equal(b.size, 0);
    assert_int_equal(b.capacity, 0);
    assert_null(b.items);

    releaseArray_int(&b);
}


void testAddToArray(void **state) {
    Array_int a = makeArray_int();
    assert_int_equal(a.size, 0);

    int n = 1;
    for (int i = 0; i < 8; ++i, ++n) {
        int *pi = addToArray_int(&a, n);
        assert_ptr_equal(pi, a.items + a.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 8);
    }
    for (int i = 0; i < 8; ++i, ++n) {
        int *pi = addToArray_int(&a, n);
        assert_ptr_equal(pi, a.items + a.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 16);
    }
    for (int i = 0; i < 16; ++i, ++n) {
        int *pi = addToArray_int(&a, n);
        assert_ptr_equal(pi, a.items + a.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 32);
    }
    for (int i = 0; i < 32; ++i, ++n) {
        int *pi = addToArray_int(&a, n);
        assert_ptr_equal(pi, a.items + a.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 64);
    }
    for (int i = 0; i < 64; ++i, ++n) {
        int *pi = addToArray_int(&a, n);
        assert_ptr_equal(pi, a.items + a.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 128);
    }

    for (int i = 0; i < 128; ++i)
        assert_int_equal(a.items[i], i + 1);

    releaseArray_int(&a);
}


void testAddByPtrToArray(void **state) {
    Array_int a = makeArray_int();
    assert_int_equal(a.size, 0);

    int n = 1;
    for (int i = 0; i < 8; ++i, ++n) {
        addByPtrToArray_int(&a, &n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 8);
    }
    for (int i = 0; i < 8; ++i, ++n) {
        addByPtrToArray_int(&a, &n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 16);
    }
    for (int i = 0; i < 16; ++i, ++n) {
        addByPtrToArray_int(&a, &n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 32);
    }
    for (int i = 0; i < 32; ++i, ++n) {
        addByPtrToArray_int(&a, &n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 64);
    }
    for (int i = 0; i < 64; ++i, ++n) {
        addByPtrToArray_int(&a, &n);
        assert_int_equal(a.size, n);
        assert_int_equal(a.capacity, 128);
    }

    for (int i = 0; i < 128; ++i)
        assert_int_equal(a.items[i], i + 1);

    releaseArray_int(&a);
}


void testReduceArray(void **state) {
    Array_int a = makeSizedArray_int(12345);
    for (int i = 0; i < 12345; ++i)
        a.items[i] = i;
    assert_int_equal(a.size, 12345);
    assert_int_equal(a.capacity, 12345);

    a.size = 100;
    reduceArray_int(&a);
    assert_int_equal(a.capacity, 256);

    releaseArray_int(&a);
}
