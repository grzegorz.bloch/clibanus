#ifndef FMT_H
#define FMT_H


#include <stddef.h>
#include <stdint.h>
#include <stdio.h>


/**
 * Formats bytes as numbers,
 * i.e. starting from the most significant bits.
 */


int sprinth(
    char *buffer,
    void *bytes,
    int numBytes,
    int numSeparated
);


int fprinth(
    FILE *fp,
    void *bytes,
    int numBytes,
    int numSeparated
);

int printh(
    void *bytes,
    int numBytes,
    int numSeparated
);


int fprinthln(
    FILE *fp,
    void *bytes,
    int numBytes,
    int numSeparated
);

int printhln(
    void *bytes,
    int numBytes,
    int numSeparated
);


int printh_uint32_t(uint32_t word);

int printh_uint64_t(uint64_t word);


#endif // FMT_H
