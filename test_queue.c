#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "queue/queue_int.h"


void testInitQueue(void **state) {
    Queue_int q;
    initQueue_int(&q, 543);
    assert_int_equal(q.size, 0);
    assert_int_equal(q.capacity, 543);
    assert_int_equal(q.minCapacity, 543);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 542);
    assert_non_null(q.items);

    releaseQueue_int(&q);
}


void testReleaseQueue(void **state) {
    Queue_int q;
    initQueue_int(&q, 7);

    releaseQueue_int(&q);
    assert_int_equal(q.size, 0);
    assert_int_equal(q.capacity, 0);
    assert_int_equal(q.minCapacity, 0);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 0);
    assert_null(q.items);
}


void testMakeQueue(void **state) {
    Queue_int q = makeQueue_int();
    assert_int_equal(q.size, 0);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 7);
    assert_non_null(q.items);

    releaseQueue_int(&q);
}


void testMakeSizedQueue(void **state) {
    Queue_int q = makeSizedQueue_int(234);
    assert_int_equal(q.size, 0);
    assert_int_equal(q.capacity, 468);
    assert_int_equal(q.minCapacity, 468);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 467);
    assert_non_null(q.items);

    releaseQueue_int(&q);
}


void testCopyQueue(void **state) {
    Queue_int q = makeSizedQueue_int(54);
    for (int i = 0; i < 54; ++i)
        addToQueue_int(&q, i);

    Queue_int r = copyQueue_int(&q);

    assert_int_equal(r.size, 54);
    assert_int_equal(r.capacity, 108);
    assert_int_equal(r.minCapacity, 108);
    assert_non_null(r.items);
    assert_ptr_not_equal(q.items, r.items);
    assert_memory_equal(q.items, r.items, sizeof(int) * 108);

    releaseQueue_int(&r);
    releaseQueue_int(&q);
}


void testCopyReleasedQueue(void **state) {
    Queue_int q = makeSizedQueue_int(54);
    releaseQueue_int(&q);

    Queue_int r = copyQueue_int(&q);
    assert_int_equal(r.size, 0);
    assert_int_equal(r.capacity, 0);
    assert_int_equal(r.minCapacity, 0);
    assert_null(r.items);

    releaseQueue_int(&r);
}


void testAddToQueue(void **state) {
    Queue_int q = makeQueue_int();
    assert_int_equal(q.size, 0);

    int n = 1;
    for (int i = 0; i < 8; ++i, ++n) {
        int *pi = addToQueue_int(&q, n);
        assert_ptr_equal(pi, q.items + q.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(q.capacity, 8);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.size, n);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 8; ++i, ++n) {
        int *pi = addToQueue_int(&q, n);
        assert_ptr_equal(pi, q.items + q.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 16);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 16; ++i, ++n) {
        int *pi = addToQueue_int(&q, n);
        assert_ptr_equal(pi, q.items + q.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 32);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 32; ++i, ++n) {
        int *pi = addToQueue_int(&q, n);
        assert_ptr_equal(pi, q.items + q.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 64);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 64; ++i, ++n) {
        int *pi = addToQueue_int(&q, n);
        assert_ptr_equal(pi, q.items + q.size - 1);
        assert_int_equal(*pi, n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 128);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }

    for (int i = 0; i < 128; ++i)
        assert_int_equal(q.items[i], i + 1);

    releaseQueue_int(&q);
}


void testAddByPtrToQueue(void **state) {
    Queue_int q = makeQueue_int();
    assert_int_equal(q.size, 0);

    int n = 1;
    for (int i = 0; i < 8; ++i, ++n) {
        addByPtrToQueue_int(&q, &n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 8);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 8; ++i, ++n) {
        addByPtrToQueue_int(&q, &n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 16);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 16; ++i, ++n) {
        addByPtrToQueue_int(&q, &n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 32);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 32; ++i, ++n) {
        addByPtrToQueue_int(&q, &n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 64);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }
    for (int i = 0; i < 64; ++i, ++n) {
        addByPtrToQueue_int(&q, &n);
        assert_int_equal(q.size, n);
        assert_int_equal(q.capacity, 128);
        assert_int_equal(q.minCapacity, 8);
        assert_int_equal(q.head, 0);
        assert_int_equal(q.tail, n - 1);
    }

    for (int i = 0; i < 128; ++i)
        assert_int_equal(q.items[i], i + 1);

    releaseQueue_int(&q);
}


void testRemoveFromQueue(void **state) {
    Queue_int q = makeQueue_int();

    addToQueue_int(&q, 1);
    addToQueue_int(&q, 2);
    addToQueue_int(&q, 5);

    assert_int_equal(q.size, 3);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 2);
    assert_int_equal(getFromQueue_int(&q), 1);

    int item = 0;
    assert_int_equal(removeFromQueue_int(&q, &item), 0);

    assert_int_equal(item, 1);
    assert_int_equal(q.size, 2);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 1);
    assert_int_equal(q.tail, 2);
    assert_int_equal(getFromQueue_int(&q), 2);

    assert_int_equal(removeFromQueue_int(&q, &item), 0);

    assert_int_equal(item, 2);
    assert_int_equal(q.size, 1);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 2);
    assert_int_equal(q.tail, 2);
    assert_int_equal(getFromQueue_int(&q), 5);

    assert_int_equal(removeFromQueue_int(&q, &item), 0);

    assert_int_equal(item, 5);
    assert_int_equal(q.size, 0);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 3);
    assert_int_equal(q.tail, 2);

    assert_int_equal(removeFromQueue_int(&q, &item), -1);
    assert_int_equal(item, 5);
    assert_int_equal(q.size, 0);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 3);
    assert_int_equal(q.tail, 2);

    releaseQueue_int(&q);
}


void testExtendStraightQueue(void **state) {
    Queue_int q = makeQueue_int();

    for (int i = 0; i < 8; ++i)
        assert_non_null(addToQueue_int(&q, i + 1));
    assert_int_equal(q.size, 8);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 7);
    for (int i = 0; i < 8; ++i)
        assert_int_equal(q.items[i], i + 1);

    addToQueue_int(&q, 9);
    assert_int_equal(q.size, 9);
    assert_int_equal(q.capacity, 16);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 8);
    for (int i = 0; i < 9; ++i)
        assert_int_equal(q.items[i], i + 1);

    releaseQueue_int(&q);
}


void testExtendRoundedQueue(void **state) {
    Queue_int q = makeQueue_int();

    for (int i = 0; i < 8; ++i)
        assert_non_null(addToQueue_int(&q, i + 1));
    for (int i = 0; i < 5; ++i)
        assert_int_equal(removeFromQueue_int(&q, NULL), 0);
    for (int i = 0; i < 5; ++i)
        assert_non_null(addToQueue_int(&q, 11 + i));

    assert_int_equal(q.size, 8);
    assert_int_equal(q.capacity, 8);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 5);
    assert_int_equal(q.tail, 4);

    assert_int_equal(q.items[0], 11);
    assert_int_equal(q.items[1], 12);
    assert_int_equal(q.items[2], 13);
    assert_int_equal(q.items[3], 14);
    assert_int_equal(q.items[4], 15);
    assert_int_equal(q.items[5], 6);
    assert_int_equal(q.items[6], 7);
    assert_int_equal(q.items[7], 8);

    assert_non_null(addToQueue_int(&q, 101));

    /*
    printHexNum(q.items, sizeof(int) * q.size, sizeof(int));
    printf("\n");
    */

    assert_int_equal(q.size, 9);
    assert_int_equal(q.capacity, 16);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 8);

    assert_int_equal(q.items[0], 6);
    assert_int_equal(q.items[1], 7);
    assert_int_equal(q.items[2], 8);
    assert_int_equal(q.items[3], 11);
    assert_int_equal(q.items[4], 12);
    assert_int_equal(q.items[5], 13);
    assert_int_equal(q.items[6], 14);
    assert_int_equal(q.items[7], 15);
    assert_int_equal(q.items[8], 101);

    releaseQueue_int(&q);
}


void testContractStraightQueue(void **state) {
    Queue_int q = makeQueue_int();

    for (int i = 0; i < 128; ++i)
       assert_non_null(addToQueue_int(&q, i + 1));
    assert_int_equal(q.size, 128);
    assert_int_equal(q.capacity, 128);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 127);
    for (int i = 0; i < 128; ++i)
        assert_int_equal(q.items[i], i + 1);

    for (int i = 0; i < 64 + 31; ++i)
        assert_int_equal(removeFromQueue_int(&q, NULL), 0);

    assert_int_equal(q.size, 33);
    assert_int_equal(q.capacity, 128);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 95);
    assert_int_equal(q.tail, 127);
    for (int i = 95; i < 128; ++i)
        assert_int_equal(q.items[i], i + 1);

    assert_int_equal(removeFromQueue_int(&q, NULL), 0);

    assert_int_equal(q.size, 32);
    assert_int_equal(q.capacity, 64);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 31);
    for (int i = 0; i < 32; ++i)
        assert_int_equal(q.items[i], i + 97);

    releaseQueue_int(&q);
}


void testContractRoundedQueue(void **state) {
    Queue_int q = makeQueue_int();

    for (int i = 0; i < 64; ++i)
       assert_non_null(addToQueue_int(&q, i + 1));
    assert_int_equal(q.size, 64);
    assert_int_equal(q.capacity, 64);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 63);
    for (int i = 0; i < 64; ++i)
        assert_int_equal(q.items[i], i + 1);

    for (int i = 0; i < 10; ++i)
        assert_int_equal(removeFromQueue_int(&q, NULL), 0);
    for (int i = 0; i < 10; ++i)
       assert_non_null(addToQueue_int(&q, i + 101));
    assert_int_equal(q.size, 64);
    assert_int_equal(q.capacity, 64);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 10);
    assert_int_equal(q.tail, 9);
    for (int i = 0; i < 10; ++i)
        assert_int_equal(q.items[i], i + 101);
    for (int i = 10; i < 64; ++i)
        assert_int_equal(q.items[i], i + 1);

    for (int i = 0; i < 32 + 15; ++i)
        assert_int_equal(removeFromQueue_int(&q, NULL), 0);
    assert_int_equal(q.size, 17);
    assert_int_equal(q.capacity, 64);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 57);
    assert_int_equal(q.tail, 9);
    for (int i = 0; i < 10; ++i)
        assert_int_equal(q.items[i], i + 101);
    for (int i = 57; i < 64; ++i)
        assert_int_equal(q.items[i], i + 1);

    assert_int_equal(removeFromQueue_int(&q, NULL), 0);
    assert_int_equal(q.size, 16);
    assert_int_equal(q.capacity, 32);
    assert_int_equal(q.minCapacity, 8);
    assert_int_equal(q.head, 0);
    assert_int_equal(q.tail, 15);

    for (int i = 0; i < 6; ++i)
        assert_int_equal(q.items[i], i + 59);
    for (int i = 6; i < 16; ++i)
        assert_int_equal(q.items[i], i + 95);

    releaseQueue_int(&q);
}


void testGetFromQueue(void **state) {
    Queue_int q = makeQueue_int();

    for (int i = 0; i < 100; ++i)
       assert_non_null(addToQueue_int(&q, i + 1));
    for (int i = 0; i < 100; ++i) {
        assert_int_equal(getFromQueue_int(&q), i + 1);
        assert_int_equal(removeFromQueue_int(&q, NULL), 0);
    }

    releaseQueue_int(&q);
}


void testGetPtrFromQueue(void **state) {
    Queue_int q = makeQueue_int();

    for (int i = 0; i < 100; ++i)
       assert_non_null(addToQueue_int(&q, i + 1));
    for (int i = 0; i < 100; ++i) {
        int *pi = getPtrFromQueue_int(&q);
        assert_non_null(pi);
        if (q.capacity == 128) {
            assert_int_equal(i, q.head);
            assert_ptr_equal(pi, q.items + i);
        }
        else if (q.capacity == 64) {
            assert_int_equal(i - (100 - 32), q.head);
            assert_ptr_equal(pi, q.items + i - (100 - 32));
        }
        else if (q.capacity == 32) {
            assert_int_equal(i - (100 - 16), q.head);
            assert_ptr_equal(pi, q.items + i - (100 - 16));
        }
        else if (q.capacity == 16) {
            assert_int_equal(i - (100 - 8), q.head);
            assert_ptr_equal(pi, q.items + i - (100 - 8));
        }
        else {
            assert_int_equal(i - (100 - 4), q.head);
            assert_ptr_equal(pi, q.items + i - (100 - 4));
        }
        assert_int_equal(*pi, i + 1);
        assert_int_equal(removeFromQueue_int(&q, NULL), 0);
    }

    releaseQueue_int(&q);
}
