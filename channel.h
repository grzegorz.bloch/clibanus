/**
 * Generically typed, fixed capacity, thread-safe, blocking channel.
 */
#include <stddef.h>
#include <pthread.h>

#include "channel_def.h"


struct CHANNEL {
    CHANNEL_ITEM *items;
    size_t capacity;
    /* Atomic size to allow for non-blocking emptiness check. */
    _Atomic size_t size;
    size_t head;
    size_t tail;

    pthread_mutex_t lock;
    pthread_cond_t notEmpty;
    pthread_cond_t notFull;
};
typedef struct CHANNEL CHANNEL;


int INIT_CHANNEL(CHANNEL *ch, size_t capacity);

int RELEASE_CHANNEL(CHANNEL *ch);

CHANNEL MAKE_CHANNEL(size_t capacity);


void ADD_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM item);

int ADD_TIMED_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM item, long ms);

void ADD_BY_PTR_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM *item);

int ADD_TIMED_BY_PTR_TO_CHANNEL(CHANNEL *ch, CHANNEL_ITEM *item, long ms);

CHANNEL_ITEM GET_FROM_CHANNEL(CHANNEL *ch);

CHANNEL_ITEM GET_TIMED_FROM_CHANNEL(CHANNEL *ch, long ms);


#include "channel_undef.h"
