CC = gcc
CFLAGS = -Wall -O3


objects = array/array_int.o \
	array/array_char.o \
	array/array_long.o \
	array/array_double.o \
	array/array_float.o \
	array/array_int8_t.o \
	array/array_int16_t.o \
	array/array_int32_t.o \
	array/array_int64_t.o \
	array/array_uint8_t.o \
	array/array_uint16_t.o \
	array/array_uint32_t.o \
	array/array_uint64_t.o \
	array/array_array_uint64_t.o \
	map/map_int_ptr.o \
	map/map_int_int.o \
	map/map_uint64_t_uint64_t.o \
	map/map_uint64_t_uint8_t.o \
	map/map_int_char.o \
	map/map_intint_int.o \
	queue/queue_int.o \
	queue/queue_char.o \
	queue/queue_long.o \
	queue/queue_double.o \
	queue/queue_float.o \
	channel/channel_int.o \
	channel/channel_char.o \
	channel/channel_long.o \
	channel/channel_double.o \
	channel/channel_float.o \
	dset/dset_int.o \
	fmt.o \
	logg.o


array_def.h: token.h
array.h: array_def.h array_undef.h
array.c: array_def.h array_undef.h array.h

map_def.h: token.h
map.h: map_def.h map_undef.h
map.c: map_def.h map_undef.h map.h

QueuDefine.h: token.h
queue.h: queue_def.h queue_undef.h
queue.c: queue_def.h queue_undef.h queue.h

channel_def.h: token.h
channel.h: channel_def.h channel_undef.h
channel.c: channel_def.h channel_undef.h channel.h


dset/dset_int.h: map.h
dset/dset_int.c: dset/dset_int.h

fmt.c: fmt.h
logg.c: logg.h fmt.h


array/array_int.o: array/array_int.h array.h array.c
array/array_char.o: array/array_char.h array.h array.c
array/array_long.o: array/array_long.h array.h array.c
array/array_double.o: array/array_double.h array.h array.c
array/array_float.o: array/array_float.h array.h array.c
array/array_int8_t.o: array/array_int8_t.h array.h array.c
array/array_int16_t.o: array/array_int16_t.h array.h array.c
array/array_int32_t.o: array/array_int32_t.h array.h array.c
array/array_int64_t.o: array/array_int64_t.h array.h array.c
array/array_uint8_t.o: array/array_uint8_t.h array.h array.c
array/array_uint16_t.o: array/array_uint16_t.h array.h array.c
array/array_uint32_t.o: array/array_uint32_t.h array.h array.c
array/array_uint64_t.o: array/array_uint64_t.h array.h array.c
array/array_array_uint64_t.o: array/array_array_uint64_t.h array.h array.c
map/map_int_ptr.o: map/map_int_ptr.h map.h map.c
map/map_int_int.o: map/map_int_int.h map.h map.c
map/map_uint64_t_uint64_t.o: map/map_uint64_t_uint64_t.h map.h map.c
map/map_uint64_t_uint8_t.o: map/map_uint64_t_uint8_t.h map.h map.c
map/map_int_char.o: map/map_int_char.h map.h map.c
map/map_intint_int.o: pair/int_int.h map/map_intint_int.h map.h map.c
queue/queue_int.o: queue/queue_int.h queue.h queue.c
queue/queue_char.o: queue/queue_char.h queue.h queue.c
queue/queue_long.o: queue/queue_long.h queue.h queue.c
queue/queue_double.o: queue/queue_double.h queue.h queue.c
queue/queue_float.o: queue/queue_float.h queue.h queue.c
channel/channel_int.o: channel/channel_int.h channel.h channel.c
channel/channel_char.o: channel/channel_char.h channel.h channel.c
channel/channel_long.o: channel/channel_long.h channel.h channel.c
channel/channel_double.o: channel/channel_double.h channel.h channel.c
channel/channel_float.o: channel/channel_float.h channel.h channel.c
dset/dset_int.o: dset/dset_int.h map.h map.c
fmt.o: fmt.h
logg.o: logg.h


libs = fmt.a \
	fmt.a \
	logg.a \
	array/libarray_int.a \
	array/libarray_char.a \
	array/libarray_long.a \
	array/libarray_double.a \
	array/libarray_float.a \
	array/libarray_int8_t.a \
	array/libarray_int16_t.a \
	array/libarray_int32_t.a \
	array/libarray_int64_t.a \
	array/libarray_uint8_t.a \
	array/libarray_uint16_t.a \
	array/libarray_uint32_t.a \
	array/libarray_uint64_t.a \
	array/libarray_array_uint64_t.a \
	map/libmap_int_ptr.a \
	map/libmap_int_int.a \
	map/libmap_uint64_t_uint64_t.a \
	map/libmap_uint64_t_uint8_t.a \
	map/libmap_int_char.a \
	map/libmap_intint_int.a \
	queue/libqueue_int.a \
	queue/libqueue_char.a \
	queue/libqueue_long.a \
	queue/libqueue_double.a \
	queue/libqueue_float.a \
	channel/libchannel_int.a \
	channel/libchannel_char.a \
	channel/libchannel_long.a \
	channel/libchannel_double.a \
	channel/libchannel_float.a \
	dset/libdset_int.a


array/libarray_int.a: array/array_int.o
	ar rcs $@ $^

array/libarray_char.a: array/array_char.o
	ar rcs $@ $^

array/libarray_long.a: array/array_long.o
	ar rcs $@ $^

array/libarray_double.a: array/array_double.o
	ar rcs $@ $^

array/libarray_float.a: array/array_float.o
	ar rcs $@ $^

array/libarray_int8_t.a: array/array_int8_t.o
	ar rcs $@ $^

array/libarray_int16_t.a: array/array_int16_t.o
	ar rcs $@ $^

array/libarray_int32_t.a: array/array_int32_t.o
	ar rcs $@ $^

array/libarray_int64_t.a: array/array_int64_t.o
	ar rcs $@ $^

array/libarray_uint8_t.a: array/array_uint8_t.o
	ar rcs $@ $^

array/libarray_uint16_t.a: array/array_uint16_t.o
	ar rcs $@ $^

array/libarray_uint32_t.a: array/array_uint32_t.o
	ar rcs $@ $^

array/libarray_uint64_t.a: array/array_uint64_t.o
	ar rcs $@ $^

array/libarray_array_uint64_t.a: array/array_array_uint64_t.o
	ar rcs $@ $^

map/libmap_int_ptr.a: map/map_int_ptr.o
	ar rcs $@ $^

map/libmap_int_int.a: map/map_int_int.o
	ar rcs $@ $^

map/libmap_uint64_t_uint64_t.a: map/map_uint64_t_uint64_t.o
	ar rcs $@ $^

map/libmap_uint64_t_uint8_t.a: map/map_uint64_t_uint8_t.o
	ar rcs $@ $^

map/libmap_int_char.a: map/map_int_char.o
	ar rcs $@ $^

map/libmap_intint_int.a: map/map_intint_int.o
	ar rcs $@ $^

queue/libqueue_int.a: queue/queue_int.o
	ar rcs $@ $^

queue/libqueue_char.a: queue/queue_char.o
	ar rcs $@ $^

queue/libqueue_long.a: queue/queue_long.o
	ar rcs $@ $^

queue/libqueue_double.a: queue/queue_double.o
	ar rcs $@ $^

queue/libqueue_float.a: queue/queue_float.o
	ar rcs $@ $^

channel/libchannel_int.a: channel/channel_int.o
	ar rcs $@ $^

channel/libchannel_char.a: channel/channel_char.o
	ar rcs $@ $^

channel/libchannel_long.a: channel/channel_long.o
	ar rcs $@ $^

channel/libchannel_double.a: channel/channel_double.o
	ar rcs $@ $^

channel/libchannel_float.a: channel/channel_float.o
	ar rcs $@ $^

dset/libdset_int.a: dset/dset_int.o
	ar rcs $@ $^

libfmt.a: fmt.o
	ar rcs $@ $^

liblogg.a: logg.o fmt.o
	ar rcs $@ $^


clibanus_libs = array/libarray_int.a \
	array/libarray_char.a \
	array/libarray_long.a \
	array/libarray_double.a \
	array/libarray_float.a \
	array/libarray_int8_t.a \
	array/libarray_int16_t.a \
	array/libarray_int32_t.a \
	array/libarray_int64_t.a \
	array/libarray_uint8_t.a \
	array/libarray_uint16_t.a \
	array/libarray_uint32_t.a \
	array/libarray_uint64_t.a \
	array/libarray_array_uint64_t.a \
	map/libmap_int_ptr.a \
	map/libmap_int_int.a \
	map/libmap_uint64_t_uint64_t.a \
	map/libmap_int_char.a \
	map/libmap_intint_int.a \
	queue/libqueue_int.a \
	queue/libqueue_char.a \
	queue/libqueue_long.a \
	queue/libqueue_double.a \
	queue/libqueue_float.a \
	channel/libchannel_int.a \
	channel/libchannel_char.a \
	channel/libchannel_long.a \
	channel/libchannel_float.a \
	channel/libchannel_double.a \
	dset/libdset_int.a \
	libfmt.a \
	liblogg.a

.PHONY: clibanus
clibanus: $(clibanus_libs)


test_array.o: array.h array.c
map/map_testkey_testvalue.o: map/map_testkey_testvalue.h map.h map.c
test_map.o: map.h map.c
test_queue.o: queue.h queue.c
test_dset.o: dset/dset_int.h
test_channel.o: channel/channel_int.h

test_objects = test.o \
	test_array.o \
	map/map_testkey_testvalue.o \
	test_map.o \
	test_queue.o \
	test_dset.o \
	test_channel.o

clibanus_tests: $(test_objects) $(objects)
	$(CC) -o tests $(test_objects) $(objects) -lcmocka

.PHONY: clibanus_tests_run
clibanus_tests_run: clibanus_tests
	./tests


.PHONY: clean
clean:
	rm -f $(objects) $(libs) clibanus_tests $(test_objects) $(clibanus_libs)
