#ifndef QUEUE_DEFINE_H
#define QUEUE_DEFINE_H

/**
 * All common Queue definitions
 */


#include "token.h"


#ifndef QUEUE
#define QUEUE MAKE_SEPARATED_TOKEN(Queue, QUEUE_ITEM)
#endif /* QUEUE */


#define INIT_QUEUE MAKE_TOKEN(init, QUEUE)

#define RELEASE_QUEUE MAKE_TOKEN(release, QUEUE)

#define MAKE_QUEUE MAKE_TOKEN(make, QUEUE)

#define MAKE_SIZED_QUEUE MAKE_TOKEN(makeSized, QUEUE)

#define COPY_QUEUE MAKE_TOKEN(copy, QUEUE)

#define ADD_TO_QUEUE MAKE_TOKEN(addTo, QUEUE)

#define ADD_BY_PTR_TO_QUEUE MAKE_TOKEN(addByPtrTo, QUEUE)

#define REMOVE_FROM_QUEUE MAKE_TOKEN(removeFrom, QUEUE)

#define GET_FROM_QUEUE MAKE_TOKEN(getFrom, QUEUE)

#define GET_PTR_FROM_QUEUE MAKE_TOKEN(getPtrFrom, QUEUE)


#endif /* QUEUE_DEFINE_H */
